INSERT INTO `Country` (`name`, `code`) VALUES ('Afghanistan','AFG');
INSERT INTO `Country` (`name`, `code`) VALUES ('Albania','ALB');
INSERT INTO `Country` (`name`, `code`) VALUES ('Algeria','DZA');
INSERT INTO `Country` (`name`, `code`) VALUES ('American Samoa','ASM');
INSERT INTO `Country` (`name`, `code`) VALUES ('Andorra','AND');
INSERT INTO `Country` (`name`, `code`) VALUES ('Angola','AGO');
INSERT INTO `Country` (`name`, `code`) VALUES ('Anguilla','AIA');
INSERT INTO `Country` (`name`, `code`) VALUES ('Antarctica','ATA');
INSERT INTO `Country` (`name`, `code`) VALUES ('Argentina','ARG');
INSERT INTO `Country` (`name`, `code`) VALUES ('Armenia','ARM');
INSERT INTO `Country` (`name`, `code`) VALUES ('Aruba','ABW');
INSERT INTO `Country` (`name`, `code`) VALUES ('Australia','AUS');
INSERT INTO `Country` (`name`, `code`) VALUES ('Austria','AUT');
INSERT INTO `Country` (`name`, `code`) VALUES ('Azerbaijan','AZE');
INSERT INTO `Country` (`name`, `code`) VALUES ('Bahamas','BHS');
INSERT INTO `Country` (`name`, `code`) VALUES ('Bahrain','BHR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Bangladesh','BGD');
INSERT INTO `Country` (`name`, `code`) VALUES ('Barbados','BRB');
INSERT INTO `Country` (`name`, `code`) VALUES ('Belarus','BLR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Belgium','BEL');
INSERT INTO `Country` (`name`, `code`) VALUES ('Belize','BLZ');
INSERT INTO `Country` (`name`, `code`) VALUES ('Benin','BEN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Bermuda','BMU');
INSERT INTO `Country` (`name`, `code`) VALUES ('Bhutan','BTN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Bolivia','BOL');
INSERT INTO `Country` (`name`, `code`) VALUES ('Bosnia and Herzegovina','BIH');
INSERT INTO `Country` (`name`, `code`) VALUES ('Botswana','BWA');
INSERT INTO `Country` (`name`, `code`) VALUES ('Brazil','BRA');
INSERT INTO `Country` (`name`, `code`) VALUES ('British Indian Ocean Territory','IOT');
INSERT INTO `Country` (`name`, `code`) VALUES ('British Virgin Islands','VGB');
INSERT INTO `Country` (`name`, `code`) VALUES ('Brunei','BRN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Bulgaria','BGR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Burkina Faso','BFA');
INSERT INTO `Country` (`name`, `code`) VALUES ('Burma (Myanmar)','MMR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Burundi','BDI');
INSERT INTO `Country` (`name`, `code`) VALUES ('Cambodia','KHM');
INSERT INTO `Country` (`name`, `code`) VALUES ('Cameroon','CMR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Canada','CAN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Cape Verde','CPV');
INSERT INTO `Country` (`name`, `code`) VALUES ('Cayman Islands','CYM');
INSERT INTO `Country` (`name`, `code`) VALUES ('Central African Republic','CAF');
INSERT INTO `Country` (`name`, `code`) VALUES ('Chad','TCD');
INSERT INTO `Country` (`name`, `code`) VALUES ('Chile','CHL');
INSERT INTO `Country` (`name`, `code`) VALUES ('China','CHN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Christmas Island','CXR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Cocos (Keeling) Islands','CCK');
INSERT INTO `Country` (`name`, `code`) VALUES ('Colombia','COL');
INSERT INTO `Country` (`name`, `code`) VALUES ('Comoros','COM');
INSERT INTO `Country` (`name`, `code`) VALUES ('Republic of the Congo','COG');
INSERT INTO `Country` (`name`, `code`) VALUES ('Democratic Republic of the Congo','COD');
INSERT INTO `Country` (`name`, `code`) VALUES ('Cook Islands','COK');
INSERT INTO `Country` (`name`, `code`) VALUES ('Costa Rica','CRC');
INSERT INTO `Country` (`name`, `code`) VALUES ('Croatia','HRV');
INSERT INTO `Country` (`name`, `code`) VALUES ('Cuba','CUB');
INSERT INTO `Country` (`name`, `code`) VALUES ('Cyprus','CYP');
INSERT INTO `Country` (`name`, `code`) VALUES ('Czech Republic','CZE');
INSERT INTO `Country` (`name`, `code`) VALUES ('Denmark','DNK');
INSERT INTO `Country` (`name`, `code`) VALUES ('Djibouti','DJI');
INSERT INTO `Country` (`name`, `code`) VALUES ('Dominica','DMA');
INSERT INTO `Country` (`name`, `code`) VALUES ('Dominican Republic','DOM');
INSERT INTO `Country` (`name`, `code`) VALUES ('Timor-Leste','TLS');
INSERT INTO `Country` (`name`, `code`) VALUES ('Ecuador','ECU');
INSERT INTO `Country` (`name`, `code`) VALUES ('Egypt','EGY');
INSERT INTO `Country` (`name`, `code`) VALUES ('El Salvador','SLV');
INSERT INTO `Country` (`name`, `code`) VALUES ('Equatorial Guinea','GNQ');
INSERT INTO `Country` (`name`, `code`) VALUES ('Eritrea','ERI');
INSERT INTO `Country` (`name`, `code`) VALUES ('Estonia','EST');
INSERT INTO `Country` (`name`, `code`) VALUES ('Ethiopia','ETH');
INSERT INTO `Country` (`name`, `code`) VALUES ('Falkland Islands','FLK');
INSERT INTO `Country` (`name`, `code`) VALUES ('Faroe Islands','FRO');
INSERT INTO `Country` (`name`, `code`) VALUES ('Fiji','FJI');
INSERT INTO `Country` (`name`, `code`) VALUES ('Finland','FIN');
INSERT INTO `Country` (`name`, `code`) VALUES ('France','FRA');
INSERT INTO `Country` (`name`, `code`) VALUES ('French Polynesia','PYF');
INSERT INTO `Country` (`name`, `code`) VALUES ('Gabon','GAB');
INSERT INTO `Country` (`name`, `code`) VALUES ('Gambia','GMB');
INSERT INTO `Country` (`name`, `code`) VALUES ('Georgia','GEO');
INSERT INTO `Country` (`name`, `code`) VALUES ('Germany','DEU');
INSERT INTO `Country` (`name`, `code`) VALUES ('Ghana','GHA');
INSERT INTO `Country` (`name`, `code`) VALUES ('Gibraltar','GIB');
INSERT INTO `Country` (`name`, `code`) VALUES ('Greece','GRC');
INSERT INTO `Country` (`name`, `code`) VALUES ('Greenland','GRL');
INSERT INTO `Country` (`name`, `code`) VALUES ('Grenada','GRD');
INSERT INTO `Country` (`name`, `code`) VALUES ('Guam','GUM');
INSERT INTO `Country` (`name`, `code`) VALUES ('Guatemala','GTM');
INSERT INTO `Country` (`name`, `code`) VALUES ('Guinea','GIN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Guinea-Bissau','GNB');
INSERT INTO `Country` (`name`, `code`) VALUES ('Guyana','GUY');
INSERT INTO `Country` (`name`, `code`) VALUES ('Haiti','HTI');
INSERT INTO `Country` (`name`, `code`) VALUES ('Honduras','HND');
INSERT INTO `Country` (`name`, `code`) VALUES ('Hong Kong','HKG');
INSERT INTO `Country` (`name`, `code`) VALUES ('Hungary','HUN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Iceland','IS');
INSERT INTO `Country` (`name`, `code`) VALUES ('India','IND');
INSERT INTO `Country` (`name`, `code`) VALUES ('Indonesia','IDN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Iran','IRN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Iraq','IRQ');
INSERT INTO `Country` (`name`, `code`) VALUES ('Ireland','IRL');
INSERT INTO `Country` (`name`, `code`) VALUES ('Isle of Man','IMN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Israel','ISR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Italy','ITA');
INSERT INTO `Country` (`name`, `code`) VALUES ('Ivory Coast','CIV');
INSERT INTO `Country` (`name`, `code`) VALUES ('Jamaica','JAM');
INSERT INTO `Country` (`name`, `code`) VALUES ('Japan','JPN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Jersey','JEY');
INSERT INTO `Country` (`name`, `code`) VALUES ('Jordan','JOR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Kazakhstan','KAZ');
INSERT INTO `Country` (`name`, `code`) VALUES ('Kenya','KEN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Kiribati','KIR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Kuwait','KWT');
INSERT INTO `Country` (`name`, `code`) VALUES ('Kyrgyzstan','KGZ');
INSERT INTO `Country` (`name`, `code`) VALUES ('Laos','LAO');
INSERT INTO `Country` (`name`, `code`) VALUES ('Latvia','LVA');
INSERT INTO `Country` (`name`, `code`) VALUES ('Lebanon','LBN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Lesotho','LSO');
INSERT INTO `Country` (`name`, `code`) VALUES ('Liberia','LBR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Libya','LBY');
INSERT INTO `Country` (`name`, `code`) VALUES ('Liechtenstein','LIE');
INSERT INTO `Country` (`name`, `code`) VALUES ('Lithuania','LTU');
INSERT INTO `Country` (`name`, `code`) VALUES ('Luxembourg','LUX');
INSERT INTO `Country` (`name`, `code`) VALUES ('Macau','MAC');
INSERT INTO `Country` (`name`, `code`) VALUES ('Macedonia','MKD');
INSERT INTO `Country` (`name`, `code`) VALUES ('Madagascar','MDG');
INSERT INTO `Country` (`name`, `code`) VALUES ('Malawi','MWI');
INSERT INTO `Country` (`name`, `code`) VALUES ('Malaysia','MYS');
INSERT INTO `Country` (`name`, `code`) VALUES ('Maldives','MDV');
INSERT INTO `Country` (`name`, `code`) VALUES ('Mali','MLI');
INSERT INTO `Country` (`name`, `code`) VALUES ('Malta','MLT');
INSERT INTO `Country` (`name`, `code`) VALUES ('Marshall Islands','MHL');
INSERT INTO `Country` (`name`, `code`) VALUES ('Mauritania','MRT');
INSERT INTO `Country` (`name`, `code`) VALUES ('Mauritius','MUS');
INSERT INTO `Country` (`name`, `code`) VALUES ('Mayotte','MYT');
INSERT INTO `Country` (`name`, `code`) VALUES ('Mexico','MEX');
INSERT INTO `Country` (`name`, `code`) VALUES ('Micronesia','FSM');
INSERT INTO `Country` (`name`, `code`) VALUES ('Moldova','MDA');
INSERT INTO `Country` (`name`, `code`) VALUES ('Monaco','MCO');
INSERT INTO `Country` (`name`, `code`) VALUES ('Mongolia','MNG');
INSERT INTO `Country` (`name`, `code`) VALUES ('Montenegro','MNE');
INSERT INTO `Country` (`name`, `code`) VALUES ('Montserrat','MSR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Morocco','MAR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Mozambique','MOZ');
INSERT INTO `Country` (`name`, `code`) VALUES ('Namibia','NAM');
INSERT INTO `Country` (`name`, `code`) VALUES ('Nauru','NRU');
INSERT INTO `Country` (`name`, `code`) VALUES ('Nepal','NPL');
INSERT INTO `Country` (`name`, `code`) VALUES ('Netherlands','NLD');
INSERT INTO `Country` (`name`, `code`) VALUES ('Netherlands Antilles','ANT');
INSERT INTO `Country` (`name`, `code`) VALUES ('New Caledonia','NCL');
INSERT INTO `Country` (`name`, `code`) VALUES ('New Zealand','NZL');
INSERT INTO `Country` (`name`, `code`) VALUES ('Nicaragua','NIC');
INSERT INTO `Country` (`name`, `code`) VALUES ('Niger','NER');
INSERT INTO `Country` (`name`, `code`) VALUES ('Nigeria','NGA');
INSERT INTO `Country` (`name`, `code`) VALUES ('Niue','NIU');
INSERT INTO `Country` (`name`, `code`) VALUES ('Norfolk','NFK');
INSERT INTO `Country` (`name`, `code`) VALUES ('Northern Mariana Islands','MNP');
INSERT INTO `Country` (`name`, `code`) VALUES ('North Korea','PRK');
INSERT INTO `Country` (`name`, `code`) VALUES ('Norway','NOR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Oman','OMN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Pakistan','PAK');
INSERT INTO `Country` (`name`, `code`) VALUES ('Palau','PLW');
INSERT INTO `Country` (`name`, `code`) VALUES ('Panama','PAN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Papua New Guinea','PNG');
INSERT INTO `Country` (`name`, `code`) VALUES ('Paraguay','PRY');
INSERT INTO `Country` (`name`, `code`) VALUES ('Peru','PER');
INSERT INTO `Country` (`name`, `code`) VALUES ('Philippines','PHL');
INSERT INTO `Country` (`name`, `code`) VALUES ('Pitcairn Islands','PCN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Poland','POL');
INSERT INTO `Country` (`name`, `code`) VALUES ('Portugal','PRT');
INSERT INTO `Country` (`name`, `code`) VALUES ('Puerto Rico','PRI');
INSERT INTO `Country` (`name`, `code`) VALUES ('Qatar','QAT');
INSERT INTO `Country` (`name`, `code`) VALUES ('Romania','ROU');
INSERT INTO `Country` (`name`, `code`) VALUES ('Russia','RUS');
INSERT INTO `Country` (`name`, `code`) VALUES ('Rwanda','RWA');
INSERT INTO `Country` (`name`, `code`) VALUES ('Saint Barthelemy','BLM');
INSERT INTO `Country` (`name`, `code`) VALUES ('Samoa','WSM');
INSERT INTO `Country` (`name`, `code`) VALUES ('San Marino','SMR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Sao Tome and Principe','STP');
INSERT INTO `Country` (`name`, `code`) VALUES ('Saudi Arabia','SAU');
INSERT INTO `Country` (`name`, `code`) VALUES ('Senegal','SEN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Serbia','SRB');
INSERT INTO `Country` (`name`, `code`) VALUES ('Seychelles','SYC');
INSERT INTO `Country` (`name`, `code`) VALUES ('Sierra Leone','SLE');
INSERT INTO `Country` (`name`, `code`) VALUES ('Singapore','SGP');
INSERT INTO `Country` (`name`, `code`) VALUES ('Slovakia','SVK');
INSERT INTO `Country` (`name`, `code`) VALUES ('Slovenia','SVN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Solomon Islands','SLB');
INSERT INTO `Country` (`name`, `code`) VALUES ('Somalia','SOM');
INSERT INTO `Country` (`name`, `code`) VALUES ('South Africa','ZAF');
INSERT INTO `Country` (`name`, `code`) VALUES ('South Korea','KOR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Spain','ESP');
INSERT INTO `Country` (`name`, `code`) VALUES ('Sri Lanka','LKA');
INSERT INTO `Country` (`name`, `code`) VALUES ('Saint Helena','SHN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Saint Kitts and Nevis','KNA');
INSERT INTO `Country` (`name`, `code`) VALUES ('Saint Lucia','LCA');
INSERT INTO `Country` (`name`, `code`) VALUES ('Saint Martin','MAF');
INSERT INTO `Country` (`name`, `code`) VALUES ('Saint Pierre and Miquelon','SPM');
INSERT INTO `Country` (`name`, `code`) VALUES ('Saint Vincent and the Grenadines','VCT');
INSERT INTO `Country` (`name`, `code`) VALUES ('Sudan','SDN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Suriname','SUR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Svalbard','SJM');
INSERT INTO `Country` (`name`, `code`) VALUES ('Swaziland','SWZ');
INSERT INTO `Country` (`name`, `code`) VALUES ('Sweden','SWE');
INSERT INTO `Country` (`name`, `code`) VALUES ('Switzerland','CHE');
INSERT INTO `Country` (`name`, `code`) VALUES ('Syria','SYR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Taiwan','TWN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Tajikistan','TJK');
INSERT INTO `Country` (`name`, `code`) VALUES ('Tanzania','TZA');
INSERT INTO `Country` (`name`, `code`) VALUES ('Thailand','THA');
INSERT INTO `Country` (`name`, `code`) VALUES ('Togo','TGO');
INSERT INTO `Country` (`name`, `code`) VALUES ('Tokelau','TKL');
INSERT INTO `Country` (`name`, `code`) VALUES ('Tonga','TON');
INSERT INTO `Country` (`name`, `code`) VALUES ('Trinidad and Tobago','TTO');
INSERT INTO `Country` (`name`, `code`) VALUES ('Tunisia','TUN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Turkey','TUR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Turkmenistan','TKM');
INSERT INTO `Country` (`name`, `code`) VALUES ('Turks and Caicos Islands','TCA');
INSERT INTO `Country` (`name`, `code`) VALUES ('Tuvalu','TUV');
INSERT INTO `Country` (`name`, `code`) VALUES ('United Arab Emirates','ARE');
INSERT INTO `Country` (`name`, `code`) VALUES ('Uganda','UGA');
INSERT INTO `Country` (`name`, `code`) VALUES ('United Kingdom','GBR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Ukraine','UKR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Uruguay','URY');
INSERT INTO `Country` (`name`, `code`) VALUES ('United States','USA');
INSERT INTO `Country` (`name`, `code`) VALUES ('Uzbekistan','UZB');
INSERT INTO `Country` (`name`, `code`) VALUES ('Vanuatu','VUT');
INSERT INTO `Country` (`name`, `code`) VALUES ('Holy See (Vatican City)','VAT');
INSERT INTO `Country` (`name`, `code`) VALUES ('Venezuela','VEN');
INSERT INTO `Country` (`name`, `code`) VALUES ('Vietnam','VNM');
INSERT INTO `Country` (`name`, `code`) VALUES ('US Virgin Islands','VIR');
INSERT INTO `Country` (`name`, `code`) VALUES ('Wallis and Futuna','WLF');
INSERT INTO `Country` (`name`, `code`) VALUES ('Western Sahara','ESH');
INSERT INTO `Country` (`name`, `code`) VALUES ('Yemen','YEM');
INSERT INTO `Country` (`name`, `code`) VALUES ('Zambia','ZMB');
INSERT INTO `Country` (`name`, `code`) VALUES ('Zimbabwe','ZWE');
