SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `poletucha` ;
CREATE SCHEMA IF NOT EXISTS `poletucha` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `poletucha` ;

-- -----------------------------------------------------
-- Table `poletucha`.`Country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `poletucha`.`Country` ;

CREATE  TABLE IF NOT EXISTS `poletucha`.`Country` (
  `idCountry` BIGINT NOT NULL AUTO_INCREMENT ,
  `code` VARCHAR(10) NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idCountry`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `poletucha`.`City`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `poletucha`.`City` ;

CREATE  TABLE IF NOT EXISTS `poletucha`.`City` (
  `idCity` BIGINT NOT NULL AUTO_INCREMENT ,
  `idCountry` BIGINT NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idCity`) ,
  INDEX `fk_City_Land1_idx` (`idCountry` ASC) ,
  CONSTRAINT `fk_City_Land1`
    FOREIGN KEY (`idCountry` )
    REFERENCES `poletucha`.`Country` (`idCountry` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `poletucha`.`Airlines`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `poletucha`.`Airlines` ;

CREATE  TABLE IF NOT EXISTS `poletucha`.`Airlines` (
  `idAirlines` BIGINT NOT NULL AUTO_INCREMENT ,
  `idCountry` BIGINT NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idAirlines`) ,
  INDEX `fk_Airlines_Country1_idx` (`idCountry` ASC) ,
  CONSTRAINT `fk_Airlines_Country1`
    FOREIGN KEY (`idCountry` )
    REFERENCES `poletucha`.`Country` (`idCountry` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `poletucha`.`Plane`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `poletucha`.`Plane` ;

CREATE  TABLE IF NOT EXISTS `poletucha`.`Plane` (
  `idPlane` BIGINT NOT NULL AUTO_INCREMENT ,
  `idAirlines` BIGINT NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  `seats` SMALLINT NOT NULL ,
  PRIMARY KEY (`idPlane`) ,
  INDEX `fk_Plane_Airlines1_idx` (`idAirlines` ASC) ,
  CONSTRAINT `fk_Plane_Airlines1`
    FOREIGN KEY (`idAirlines` )
    REFERENCES `poletucha`.`Airlines` (`idAirlines` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `poletucha`.`Airport`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `poletucha`.`Airport` ;

CREATE  TABLE IF NOT EXISTS `poletucha`.`Airport` (
  `idAirport` VARCHAR(3) NOT NULL ,
  `idCity` BIGINT NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idAirport`) ,
  INDEX `fk_Airport_City1_idx` (`idCity` ASC) ,
  CONSTRAINT `fk_Airport_City1`
    FOREIGN KEY (`idCity` )
    REFERENCES `poletucha`.`City` (`idCity` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `poletucha`.`Flight`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `poletucha`.`Flight` ;

CREATE  TABLE IF NOT EXISTS `poletucha`.`Flight` (
  `idFlight` BIGINT NOT NULL AUTO_INCREMENT ,
  `code` VARCHAR(10) NOT NULL ,
  `airportFrom` VARCHAR(3) NOT NULL ,
  `airportTo` VARCHAR(3) NOT NULL ,
  `idPlane` BIGINT NOT NULL ,
  `departure` DATETIME NOT NULL ,
  `arrival` DATETIME NOT NULL ,
  `capacity` SMALLINT NOT NULL ,
  PRIMARY KEY (`idFlight`) ,
  INDEX `fk_Flight_AirportFrom_idx` (`airportFrom` ASC) ,
  INDEX `fk_Flight_AirportTo_idx` (`airportTo` ASC) ,
  INDEX `fk_Flight_Plane1_idx` (`idPlane` ASC) ,
  CONSTRAINT `fk_Flight_Airport1`
    FOREIGN KEY (`airportFrom` )
    REFERENCES `poletucha`.`Airport` (`idAirport` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Flight_Airport2`
    FOREIGN KEY (`airportTo` )
    REFERENCES `poletucha`.`Airport` (`idAirport` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Flight_Plane1`
    FOREIGN KEY (`idPlane` )
    REFERENCES `poletucha`.`Plane` (`idPlane` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `poletucha`.`Person`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `poletucha`.`Person` ;

CREATE  TABLE IF NOT EXISTS `poletucha`.`Person` (
  `idPerson` BIGINT NOT NULL AUTO_INCREMENT ,
  `firstname` VARCHAR(40) NOT NULL ,
  `surname` VARCHAR(40) NOT NULL ,
  `street` VARCHAR(120) NOT NULL ,
  `idCity` BIGINT NOT NULL ,
  PRIMARY KEY (`idPerson`) ,
  INDEX `fk_Person_City1_idx` (`idCity` ASC) ,
  CONSTRAINT `fk_Person_City1`
    FOREIGN KEY (`idCity` )
    REFERENCES `poletucha`.`City` (`idCity` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `poletucha`.`Order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `poletucha`.`Order` ;

CREATE  TABLE IF NOT EXISTS `poletucha`.`Order` (
  `idOrder` BIGINT NOT NULL AUTO_INCREMENT ,
  `date` TIMESTAMP NOT NULL ,
  PRIMARY KEY (`idOrder`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `poletucha`.`Personrole`
-- -----------------------------------------------------
-- Delete obsolete table if it is present.
DROP TABLE IF EXISTS `poletucha`.`PersonRole` ;
DROP TABLE IF EXISTS `poletucha`.`Personrole` ;

CREATE  TABLE IF NOT EXISTS `poletucha`.`Personrole` (
  `idPersonRole` BIGINT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idPersonRole`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `poletucha`.`Currency`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `poletucha`.`Currency` ;

CREATE  TABLE IF NOT EXISTS `poletucha`.`Currency` (
  `idCurrency` VARCHAR(3) NOT NULL ,
  `name` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`idCurrency`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `poletucha`.`Reservation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `poletucha`.`Reservation` ;

CREATE  TABLE IF NOT EXISTS `poletucha`.`Reservation` (
  `idReservation` BIGINT NOT NULL AUTO_INCREMENT ,
  `idPerson` BIGINT NOT NULL ,
  `idFlight` BIGINT NOT NULL ,
  `idOrder` BIGINT NOT NULL ,
  `idPersonRole` BIGINT NOT NULL ,
  `idCurrency` VARCHAR(3) NOT NULL ,
  `price` DOUBLE NOT NULL ,
  PRIMARY KEY (`idReservation`) ,
  INDEX `fk_Reservation_Client_idx` (`idPerson` ASC) ,
  INDEX `fk_Reservation_Flight1_idx` (`idFlight` ASC) ,
  INDEX `fk_Reservation_Order1_idx` (`idOrder` ASC) ,
  INDEX `fk_Reservation_PersonRole1_idx` (`idPersonRole` ASC) ,
  INDEX `fk_Reservation_Currency1_idx` (`idCurrency` ASC) ,
  CONSTRAINT `fk_Reservation_Client`
    FOREIGN KEY (`idPerson` )
    REFERENCES `poletucha`.`Person` (`idPerson` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Reservation_Flight1`
    FOREIGN KEY (`idFlight` )
    REFERENCES `poletucha`.`Flight` (`idFlight` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Reservation_Order1`
    FOREIGN KEY (`idOrder` )
    REFERENCES `poletucha`.`Order` (`idOrder` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Reservation_PersonRole1`
    FOREIGN KEY (`idPersonRole` )
    REFERENCES `poletucha`.`PersonRole` (`idPersonRole` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Reservation_Currency1`
    FOREIGN KEY (`idCurrency` )
    REFERENCES `poletucha`.`Currency` (`idCurrency` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE = '';
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'polapp'@'localhost' IDENTIFIED BY 'p0L4P#';
GRANT ALL ON poletucha.* TO 'polapp'@'localhost';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
