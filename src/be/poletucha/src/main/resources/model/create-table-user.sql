USE `poletucha` ;

CREATE TABLE `User` (
       user_id          bigint AUTO_INCREMENT PRIMARY KEY,
       firstname        varchar(20),
       surname          varchar(30),
       login            varchar(20) NOT NULL,
       email            varchar(20),
       password         varchar(40) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET=utf8;
