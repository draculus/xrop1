package cz.osu.poletucha.db;

public class ReservationResolved extends Reservation {

    private String personFirstname;
    private String personSurname;
    private String personRoleName;
    private String flightCode;
    private String currencyName;

    public ReservationResolved (Reservation r, Person p, Personrole pr,
                                Flight f, Currency c) {
    
        super (r.getIdPerson (), r.getIdPersonRole (), r.getIdFlight (),
               r.getIdOrder (), r.getIdCurrency (), r.getPrice ());
        this.setId (r.getId ());
        this.setPersonFirstname (p.getFirstname ());
        this.setPersonSurname (p.getSurname ());
        this.setPersonRoleName (pr.getName ());
        this.setFlightCode (f.getCode ());
        this.setCurrencyName (c.getName ());
    }

    public void setPersonFirstname (String firstname) {
        this.personFirstname = firstname;
    }

    public void setPersonSurname (String surname) {
        this.personSurname = surname;
    }

    public void setPersonRoleName (String name) {
        this.personRoleName = name;
    }

    public void setFlightCode (String code) {
        this.flightCode = code;
    }

    public void setCurrencyName (String name) {
        this.currencyName = name;
    }

    public String getPersonFirstname () {
        return this.personFirstname;
    }

    public String getPersonSurname () {
        return this.personSurname;
    }

    public String getPersonRoleName () {
        return this.personRoleName;
    }

    public String getFlightCode () {
        return this.flightCode;
    }

    public String getCurrencyName () {
        return this.currencyName;
    }

}
