package cz.osu.poletucha.db;

public class PersonResolved extends Person {

    private String cityName;

    public PersonResolved (Person p, String cityName) {
        super(p.getFirstname (),
              p.getSurname (),
              p.getStreet (),
              p.getIdCity ());
        this.setId (p.getId());
        setCityName (cityName);
    }

    public void setCityName (String cityName) {
        this.cityName = cityName;
    }

    public String getCityName () {
        return this.cityName;
    }

}