package cz.osu.poletucha.db;

public class AirportResolved extends Airport {

    private String cityName;

    public AirportResolved (Airport a, String cityName) {
        super (a.getId (),
               a.getIdCity (),
               a.getName ());
        this.setCityName (cityName);
    }

    public void setCityName (String name) {
        this.cityName = name;
    }

    public String getCityName () {
        return this.cityName;
    }

}
