package cz.osu.poletucha;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * JsonP filter - allows for JSONP calls
 * @author todi
 */
public class JSONPFilter implements Filter {

   // Attributes

   /** Logger */
   protected static Log log = LogFactory.getLog(JSONPFilter.class);

   // Public methods


   @Override
   public void destroy() {
      // EMPTY
   }

   @SuppressWarnings("unchecked")
   @Override
   public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {

      final HttpServletRequest httpRequest = (HttpServletRequest) request;
      final HttpServletResponse httpResponse = (HttpServletResponse) response;

      // allow for cross domain request
      httpResponse.setHeader("Access-Control-Allow-Origin", "*");

      // gets request parameters
      final Map<String, String[]> parms = httpRequest.getParameterMap();

      if (parms.containsKey("callback")) {
         if (log.isDebugEnabled()) {
            log.debug("Wrapping response with JSONP callback '" + parms.get("callback")[0] + "'");
         }

         final OutputStream out = httpResponse.getOutputStream();

         // wrapper start
         out.write(new String(parms.get("callback")[0] + "(").getBytes());

         // filter
         chain.doFilter(request, httpResponse);

         // wrapper end
         out.write(new String(");").getBytes());

         // set proper content type
         httpResponse.setContentType("text/javascript;charset=UTF-8");

         out.close();
      } else {
         chain.doFilter(request, response);
      }
   }

   @Override
   public void init(final FilterConfig arg0) throws ServletException {
      // EMPTY
   }
}
