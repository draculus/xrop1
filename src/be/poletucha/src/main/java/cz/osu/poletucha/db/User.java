package cz.osu.poletucha.db;
// Generated Jun 24, 2013 11:30:13 PM by Hibernate Tools 3.2.2.GA



/**
 * User generated by hbm2java
 */
public class User  implements java.io.Serializable {


     private long id;
     private String firstname;
     private String surname;
     private String email;
     private String login;
     private String password;

    public User() {
    }

    public User(String firstname, String surname, String email, String login, String password) {
       this.firstname = firstname;
       this.surname = surname;
       this.email = email;
       this.login = login;
       this.password = password;
    }
   
    public long getId() {
        return this.id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    public String getFirstname() {
        return this.firstname;
    }
    
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    public String getSurname() {
        return this.surname;
    }
    
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    public String getLogin() {
        return this.login;
    }
    
    public void setLogin(String login) {
        this.login = login;
    }
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }




}


