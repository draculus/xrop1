package cz.osu.poletucha.db;
// Generated Jan 26, 2013 11:10:52 PM by Hibernate Tools 3.2.2.GA



/**
 * Airport generated by hbm2java
 */
public class Airport  implements java.io.Serializable {


     private String id;
     private long idCity;
     private String name;

    public Airport() {
    }

	
    public Airport(String id) {
        this.id = id;
    }
    public Airport(String id, long idCity, String name) {
       this.id = id;
       this.idCity = idCity;
       this.name = name;
    }
   
    public String getId() {
        return this.id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    public long getIdCity() {
        return this.idCity;
    }
    
    public void setIdCity(long idCity) {
        this.idCity = idCity;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }




}


