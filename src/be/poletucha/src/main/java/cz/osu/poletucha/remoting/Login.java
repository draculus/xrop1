package cz.osu.poletucha.remoting;

public class Login {

    private String login;
    private String password;

    public Login () {}

    public void setLogin (String login) {
        this.login = login;
    }

    public void setPassword (String password) {
        this.password = password;
    }

    public String getLogin () {
        return this.login;
    }

    public String getPassword () {
        return this.password;
    }

}
