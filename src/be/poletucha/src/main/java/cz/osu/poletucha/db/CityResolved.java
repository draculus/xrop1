package cz.osu.poletucha.db;

public class CityResolved extends City {

    private String countryName;

    public CityResolved (City c, String n) {
        super (c.getIdCountry (), c.getName ());
        this.setId (c.getId());
        this.setCountryName (n);
    }

    public void setCountryName (String name) {
        this.countryName = name;
    }

    public String getCountryName () {
        return this.countryName;
    }

}
