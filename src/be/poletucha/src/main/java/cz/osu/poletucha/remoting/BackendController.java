package cz.osu.poletucha.remoting;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.JsonParseException;

import java.util.*;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.Query;
import org.hibernate.HibernateException;

import cz.osu.poletucha.db.*;

/**
 * Controller
 * @author Pavel Kačer
 *
 */
/* @RequestMapping(value="*") */
@Controller
public class BackendController {

   private String dbClassPath = "cz.osu.poletucha.db.";

   @RequestMapping(value = "", method = RequestMethod.GET, produces = "text/html")
   @ResponseBody
   public String getDocumentation () {
       String htmlStart = "<html>\n";
       String head = "<head>\n"
           + "<title>Information system Poletis</title>"
           + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">"
           + "</head>\n";
       String body = "<body>"
           + "<p>Welcome to Poletis backend.</p>\n"
           + "<p>What works as backend (relative to the URL of this info page).</p>\n"
           + "<table border=\"1\">\n"
           + "<tr><th>Relative URL</th><th>Description</td></th>\n"
           + "<tr><td>/get/&lt;object&gt;</td><td>List of entities values of the type specified in &lt;object&gt;. A list of objects in JSON format is returned. List of objects is below. If there is an error, empty message body is returned.</td></tr>\n"
           + "<tr><td>/get/&lt;object&gt;/detlist</td><td>The same list as above only some of the foreign key values are resolved.</td></tr>\n"
           + "<tr><td>/get/&lt;object&gt;/&lt;id&gt;</td><td>Returns a JSON object with this id from database. For example '/get/country/45' or '/get/person/3'. On error the error message is returned.</td></tr>\n"
           + "<tr><td>/get</td><td>Generic GET request to retrieve any information from database. Returns a list on success or error message on failure. You need to provide parameters 'object', 'key', 'value'. Objects are listed below. Keys are attributes of the objects (in database). To include an empty space in 'value' use '%20' or an undersore '_'. For example '/get?object=country&key=name&value=u%25' will retrieve all countries whose name starts by letter 'u'. The '/get?object=city&key=name&value=prague' returns a list of cities whose name is Prague.</td></tr>\n"
           + "<tr><td>/&lt;object&gt;/&lt;action&gt;</td><td>A POST request that does some action with object &lt;object&gt;. List of objects is below. Actions are 'add' or 'delete'. Body has to contain a requested object in JSON format. 'Content-Type' and 'Accept' has to be 'application/json'. The object is updated when using action 'add' and if object having the same ID already exists. For new object set ID to null. Returns created or updated object in JSON format within the response body if succeded, error message otherwise.</td></tr>\n"
           + "<tr><td>/login</td><td>A POST request that contains an object with attributes 'login' and 'password'. For example: { \"login\":\"jdoe\", \"password\":\"secret\" }. Returns a complete user object on success or return code 418 and message if the login and password combination is not in the database. An error message is returned when some error occures.</td></tr>\n"
           + "</table>\n"
           + "<p>"
           + "All /get requests are of type GET. The /&lt;object&gt;/&lt;action&gt; requests are POST."
           + "</p>"
           + "<p>"
           + "List of objects: 'Airlines', 'Airport', 'City', 'Country', 'Currency', 'Flight', 'Order', 'Person', 'Personrole', 'Plane', 'Reservation'."
           + "</p>"
           + "</body>\n";
       String htmlEnd = "</html>\n";
       return htmlStart + head + body + htmlEnd;
   }

   /* Methods for getting data. */

   /**
    * Returns whole tables on request.
    *
    * @return List of objects (entities).
    */
   @RequestMapping(value = "/get/{object}", method = RequestMethod.GET, produces = "application/json")
   @ResponseBody
   public Object getAllValues (@PathVariable String object) {
       List l;
       try {
           l = runSelect (object);
       } catch (HibernateException e) {
           return e.getMessage ();
       }
       return l;
   }

    /**
     * Returns whole tables on request with some foreign keys resolved.
     *
     * @return List of objects (entities).
     */
    @RequestMapping(value = "/get/{object}/detlist", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Object getDetailAllValues(@PathVariable String object) {
        List l;
        try {
            l = runSelectWithDetail (object);
        } catch (HibernateException e) {
            return e.getMessage ();
        }
        return l;
    }

    /**
     * Get a list of objects searched by key.
     *
     * @return A list of objects of the type specified in parameter or error message.
     */
   @RequestMapping (value = "/get", method = RequestMethod.GET, produces = "application/json")
   @ResponseBody
   public Object searchByParam (@RequestParam(value = "object", required = true) String object, @RequestParam(value = "key", required = true) String key, @RequestParam(value = "value", required = true) String value) {
       Session hibersession = getSession ();
       if (! hibersession.isOpen ()) {
           return "Could not connect to database.";
       }
       String tableName = capitalize (object);
       String strippedValue = stripUnderlines (value);
       Transaction t = null;
       try {
           t = hibersession.beginTransaction ();
           Query q = hibersession
               .createSQLQuery ("SELECT * FROM `" + tableName + "` WHERE `" + key + "` like ?")
               .addEntity (dbClassPath + tableName);
           q.setString (0, strippedValue);
           List l = q.list ();
           t.commit ();
           return l;
       } catch (Exception e) {
           if (t != null)
               t.rollback ();
           return e.getMessage ();
       }
   }

    /**
    * Gets object from the database.
    *
    * @return String containing object object instance in JSON format. Error message otherwise.
    */
   @RequestMapping(value = "/get/{object}/{id}", method = RequestMethod.GET, produces = "application/json")
   @ResponseBody
   public Object getObject (@PathVariable ("object") String object, @PathVariable("id") String id) {
       Session hibersession = getSession ();
       if (! hibersession.isOpen ()) {
           return "Could not connect to database.";
       }
       String tableName = capitalize (object);
       Transaction t = null;
       try {
           t = hibersession.beginTransaction ();
           Query q = hibersession
               .createSQLQuery ("SELECT * FROM `" + tableName + "` WHERE `id" + tableName + "` = ?")
               .addEntity (dbClassPath + tableName);
           q.setString (0, id);
           List l = q.list ();
           if (t != null)
               t.commit ();
           if (! l.isEmpty ()) {
               return l.get (0);
           } else {
               return "The result set is empty.";
           }
       } catch (Exception e) {
           if (t != null)
               t.rollback ();
           return e.getMessage ();
       }
   }

   @RequestMapping(value = "/{object}/{action}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
   @ResponseBody
   public Object handleObject (@RequestBody String body, @PathVariable("object") String object, @PathVariable("action") String action) {
       ObjectMapper mapper = new ObjectMapper();
       String objectName = capitalize (object);
       Object o;
       try {
           o = mapper.readValue(body, Class.forName (dbClassPath + objectName));
       } catch (ClassNotFoundException cnfe) {
           return cnfe.getMessage ();
       } catch (JsonParseException jpe) {
           return "Parsing error: " + jpe.getMessage ();
       } catch (JsonMappingException jme) {
           return "Parsing error: " + jme.getMessage ();
       } catch (IOException ioe) {
           return "Parsing error: " + ioe.getMessage ();
       }

       Session hibersession = null;
       try {
           hibersession = getSession ();
           if (! hibersession.isOpen ()) {
               return "The session is currently closed.";
           }
       } catch (Exception e) {
           return e.getMessage ();
       }
       Transaction t = null;
       try {
           t = hibersession.beginTransaction ();
           if (action.equalsIgnoreCase ("add")) {
               hibersession.saveOrUpdate (o);
           } else if (action.equalsIgnoreCase ("delete")) {
               hibersession.delete (o);
           } else {
               if (t != null)
                   t.rollback ();
               return "Unknown action. Only 'add' or 'delete' are allowed.";
           }
           t.commit ();
       } catch (Exception e) {
           if (t != null)
               t.rollback ();
           return "Transaction processing error: " + e.getMessage ();
       }

       try {
           return mapper.writeValueAsString (o);
       } catch (JsonMappingException jme) {
           return "Serializing error: " + jme.getMessage ();
       } catch (JsonGenerationException jge) {
           return "Serializing error: " + jge.getMessage ();
       } catch (IOException ioe) {
           return "Serializing error: " + ioe.getMessage ();
       }
   }

   @RequestMapping(value = "/search/{object}", method = RequestMethod.GET, produces = "application/json")
   @ResponseBody
   public Object searchObject (@PathVariable String object, HttpServletRequest req) {
       Session ses = getSession ();
       if (! ses.isOpen ()) {
           return "Could not connect to database.";
       }

       String tableName = capitalize (object);
       Map<String, String[]> params = req.getParameterMap ();
       Set<String> keys = params.keySet ();
       Map<String, String> flatParams = new HashMap<String, String>();
       Transaction t = null;
       try {
           t = ses.beginTransaction ();
           StringBuilder sql = new StringBuilder ("SELECT * FROM `" + tableName + "`");
           if (keys.size () > 0) {
               sql.append (" WHERE");
               for (String key : keys) {
                   String[] parVals = params.get (key);
                   if (parVals.length > 0) {
                       flatParams.put (key, parVals[0]);
                       if (sql.lastIndexOf ("WHERE") < sql.length () - 5) {
                           sql.append (" AND");
                       }
                       if (key.compareToIgnoreCase ("arrival") == 0
                           || key.compareToIgnoreCase ("departure") == 0) {
                           sql.append (" `" + key + "` = :" + key);
                       } else {
                           sql.append (" `" + key + "` LIKE :" + key);
                       }
                   }
               }
           }
           Query q = ses.createSQLQuery (sql.toString ())
               .addEntity (dbClassPath + tableName);
           q.setProperties (flatParams);
           List l = q.list ();
           t.commit ();
           return l;
       } catch (Exception e) {
           if (t != null)
               t.rollback ();
           return e.getMessage ();
       }
   }

   @RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
   @ResponseBody
   public Object login (@RequestBody String body, HttpServletResponse resp) {
       ObjectMapper mapper = new ObjectMapper();
       Login log = null;
       try {
           log = (Login) mapper.readValue (body, Class.forName ("cz.osu.poletucha.remoting.Login"));
       } catch (ClassNotFoundException e) {
           return e.getMessage ();
       } catch (IOException e) {
           return e.getMessage ();
       }

       Session ses = null;
       try {
           ses = getSession ();
           if (! ses.isOpen ()) {
               return "ERROR: The DB connection was closed.";
           }
           Transaction t = null;
           try {
               t = ses.beginTransaction ();
               Query q = ses
                   .createSQLQuery ("SELECT * FROM `User` WHERE login = ? AND password = ?")
                   .addEntity (dbClassPath + "User");
               q.setString (0, log.getLogin ());
               q.setString (1, log.getPassword ());
               List l = q.list ();
               t.rollback ();
               if (l.isEmpty ()) {
                   resp.sendError (418, "The requested user is not in the database.");
                   return null;
               } else {
                   return l.get (0);
               }
           } catch (Exception e) {
               if (t != null)
                   t.rollback ();
               return e.getMessage ();
           }
       } catch (Exception e) {
           return e.getMessage ();
       }
   }

   private String capitalize (String text) {
       char[] strArray = text.toCharArray ();
       strArray[0] = Character.toUpperCase (strArray[0]);
       return new String (strArray);
   }

   private String stripUnderlines (String text) {
       return text.replace ('_', ' ');
   }

    private List runSelect (String tableName) throws HibernateException {
        Session hibersession = getSession ();
        if (! hibersession.isOpen ()) {
            throw new HibernateException ("Could not connect to database.");
        }

        String capTableName = capitalize (tableName);
        Transaction t = null;
        try {
            t = hibersession.beginTransaction ();
            Query q = hibersession
                .createSQLQuery ("SELECT * FROM `" + capTableName + "`")
                .addEntity (dbClassPath + capTableName);
            List l = q.list ();
            t.commit ();
            return l;
        } catch (HibernateException e) {
            t.rollback ();
            throw e;
        }
    }

    private List runSelectWithDetail (String tableName) throws HibernateException {
        Session sess = getSession ();
        Transaction t = null;
        List n = null;
        if (tableName.compareToIgnoreCase ("person") == 0) {
            List<Person> l = runSelect (tableName);
            sess = getSession ();
            t = sess.beginTransaction ();
            n = new ArrayList<PersonResolved> ();
            for (Person per : l) {
                long idCity = per.getIdCity ();
                City c = (City) sess.load (dbClassPath + "City", idCity);
                n.add (new PersonResolved (per, c.getName()));
            }
        } else if (tableName.compareToIgnoreCase ("city") == 0) {
            List<City> l = runSelect (tableName);
            sess = getSession ();
            t = sess.beginTransaction ();
            n = new ArrayList<CityResolved> ();
            for (City c : l) {
                long idCountry = c.getIdCountry ();
                Country coun = (Country) sess.load (dbClassPath + "Country", idCountry);
                n.add (new CityResolved (c, coun.getName()));
            }
        } else if (tableName.compareToIgnoreCase ("airport") == 0) {
            List<Airport> l = runSelect (tableName);
            sess = getSession ();
            t = sess.beginTransaction ();
            n = new ArrayList<AirportResolved> ();
            for (Airport a : l) {
                long idCity = a.getIdCity ();
                City c = (City) sess.load (dbClassPath + "City", idCity);
                n.add (new AirportResolved (a, c.getName()));
            }
        } else if (tableName.compareToIgnoreCase ("airlines") == 0) {
            List<Airlines> l = runSelect (tableName);
            sess = getSession ();
            t = sess.beginTransaction ();
            n = new ArrayList<AirlinesResolved> ();
            for (Airlines a : l) {
                long idCountry = a.getIdCountry ();
                Country c = (Country) sess.load (dbClassPath + "Country", idCountry);
                n.add (new AirlinesResolved (a, c.getName()));
            }
        } else if (tableName.compareToIgnoreCase ("plane") == 0) {
            List<Plane> l = runSelect (tableName);
            sess = getSession ();
            t = sess.beginTransaction ();
            n = new ArrayList<PlaneResolved> ();
            for (Plane p : l) {
                long idAirlines = p.getIdAirlines ();
                Airlines a = (Airlines) sess.load (dbClassPath + "Airlines", idAirlines);
                n.add (new PlaneResolved (p, a.getName()));
            }
        } else if (tableName.compareToIgnoreCase ("flight") == 0) {
            List<Flight> l = runSelect (tableName);
            sess = getSession ();
            t = sess.beginTransaction ();
            n = new ArrayList<FlightResolved> ();
            for (Flight f : l) {
                Airport from = (Airport) sess.load (dbClassPath + "Airport", f.getAirportFrom ());
                Airport to = (Airport) sess.load (dbClassPath + "Airport", f.getAirportTo ());
                Plane p = (Plane) sess.load (dbClassPath + "Plane", f.getIdPlane ());
                n.add (new FlightResolved (f, from, to, p));
            }
        } else if (tableName.compareToIgnoreCase ("reservation") == 0) {
            List<Reservation> l = runSelect (tableName);
            sess = getSession ();
            t = sess.beginTransaction ();
            n = new ArrayList<ReservationResolved> ();
            for (Reservation r : l) {
                Person p = (Person) sess.load (dbClassPath + "Person", r.getIdPerson ());
                Personrole pr = (Personrole) sess.load (dbClassPath + "Personrole", r.getIdPersonRole ());
                Flight f = (Flight) sess.load (dbClassPath + "Flight", r.getIdFlight ());
                cz.osu.poletucha.db.Currency c = (cz.osu.poletucha.db.Currency) sess.load (dbClassPath + "Currency",
                                                                                           r.getIdCurrency ());
                n.add (new ReservationResolved (r, p, pr, f, c));
            }
        }

        if (t != null)
            t.rollback ();
        return n;
    }

    private Session getSession () {
        SessionFactory f = HibernateUtils.getSessionFactory ();
        Session s = f.getCurrentSession ();
        if (! s.isOpen ()) {
            s = f.openSession ();
        }
        return s;
    }

}
