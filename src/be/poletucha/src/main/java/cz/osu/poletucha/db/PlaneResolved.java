package cz.osu.poletucha.db;

public class PlaneResolved extends Plane {

    private String airlinesName;

    public PlaneResolved (Plane p, String airlinesName) {
        super (p.getIdAirlines (),
               p.getName (),
               p.getSeats ());
	this.setId (p.getId ());
        setAirlinesName (airlinesName);
    }

    public void setAirlinesName (String name) {
        this.airlinesName = name;
    }

    public String getAirlinesName () {
        return this.airlinesName;
    }

}
