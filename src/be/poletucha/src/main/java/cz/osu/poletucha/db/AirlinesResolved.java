package cz.osu.poletucha.db;

public class AirlinesResolved extends Airlines {

    private String countryName;

    public AirlinesResolved (Airlines a, String countryName) {
        super (a.getIdCountry (),
               a.getName ());
        this.setId (a.getId ());
        this.setCountryName (countryName);
    }

    public void setCountryName (String n) {
        this.countryName = n;
    }

    public String getCountryName () {
        return this.countryName;
    }

}
