<?php
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) 
{
  exit("You can't access this file directly !");
}

$root_folder = "http://".$_SERVER['HTTP_HOST']."/poletucha/";
$system_folder = "sys/";
$application_folder = "app/";
$application_title = "poletucha";
$application_theme = "common";
$application_layout = "stdlayout";
$application_menu = "simplemenu";
$application_module = "poletucha";
$application_language = "cz";

//$database_User = "root";
//$database_Pasw = "rootpsw";
//$database_Host = "127.0.0.1";
//$database_Name = "test";
//$database_Type = "MYSQL5";

$backend_protocol = "http://";
//$backend_host = "95.82.135.34";
$backend_host = "localhost";
$backend_port = "8080";
$backend_dir = "/poletucha-0.0.1/backend";

?>