<?php

error_reporting(E_ALL);
ini_set("display_errors", True); 
date_default_timezone_set('UTC');

// load configuration
require_once("config.php");

$backend_url = $backend_protocol.$backend_host.":".$backend_port.$backend_dir;

// Define common constants
// SELF	... name of this file
define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

// EXT ... file extension
define('EXT', '.php');			

// BASEPATH	... full server path to this file
define('BASEPATH', str_replace(SELF, '', __FILE__));

// ROOT
define('ROOTPATH', $root_folder);

// SYSPATH ... path to the "system" folder
define('SYSPATH', $system_folder);

// APPPATH ... path to the "application" folder		
define('APPPATH', $application_folder);	

require_once(SYSPATH."lib/route.php");
require_once(SYSPATH."lib/routetable.php");

$routetable = new RouteTable();
$route = $routetable->GetRoute();
$params = $route->GetParameters();

$fileString = file_get_contents($backend_url."/".$params[0]."/".$params[1]);

header("Content-Type: application/json");

echo $fileString;
?>
