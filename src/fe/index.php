﻿<?php
// Load core class
require("core.php"); 

// Initialization of small framework
// (session start, error handler, config)
// $core = new Core();
$core = Core::GetInstance();

// Retrieve layout service
$layout = $core->GetLayout();

// Prepare header
$layout->header = "<h1>".$core->title."</h1>";

// Prepare menu
require_once(SYSPATH."lib/menuitem.php");

// No security
$header = new Template(APPPATH."template/header.tpl");
$header->Set("USERID",$core->security->GetUserId());
$header->Set("USERNAME",$core->security->GetUserName());
$layout->header = $header->Parse();

// Using security
/*if ($core->security->GetUserId()!=-1)
{	
	$core->menu->AddItem(new MenuItem("Faktury","invoice"));
	$core->menu->AddItem(new MenuItem("Zákazníci","customer"));	
	$core->menu->AddItem(new MenuItem("Nastavení","settings"));
	$core->menu->AddItem(new MenuItem("Odhlášení","login"));	
	$core->menu->AddItem(new MenuItem("O aplikaci","invoiceman "));	
}
else
{	
	$core->menu->AddItem(new MenuItem("Přihlášení","login"));	
}*/
	
// Render menu
//$core->menu->AddItem(new MenuItem("Faktury","invoice"));
//$layout->menu = $core->menu->Render();

// Direct menu
$menu = new Template(APPPATH."template/menu.tpl"); 
$menu->Set("USERID",$core->security->GetUserId());
$menu->Set("USERNAME",$core->security->GetUserName());
$layout->menu = $menu->Parse();

// Prepare content
$layout->body = "<div id=\"container\"> <div id=\"content\">";
$method = $core->controller->action;
$layout->body .= $core->controller->$method();
$layout->body .= "</div> </div>";

// Prepare footer
//$layout->footer = $core->GetContent('reklama.php');;

// Send output
$core->Render();

?>

