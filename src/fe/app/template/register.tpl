<table>
  <tr>
    <th>
      <label for="lfirstname">
        Jméno:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="text" maxlength="255" name="firstname" id="lfirstname" size="60" value="" />
    </td>
  </tr>
  <tr>
    <th>
      <label for="lsurname">
        Příjmení:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="text" maxlength="255" name="surname" id="lsurname" size="60" value="" />
    </td>
  </tr>

  <tr>
    <th>
      <label for="lemail">
        e-mail:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="text" maxlength="255" name="email" id="lemail" size="60" value="" />
    </td>
  </tr>

  <tr>
    <th>
      <label for="llogin">
        Login:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="text" maxlength="255" name="login" id="llogin" size="60" value="" />
    </td>
  </tr>

  <tr>
    <th>
      <label for="lpassword">
        Heslo:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="password" maxlength="255" name="password" id="lpassword" size="60" value="" />
    </td>
  </tr>

  <tr>
    <th></th>
    <td>
      <input type="submit" name="send" value="Odeslat" />
    </td>
  </tr>
</table>
