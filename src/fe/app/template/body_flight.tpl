<td>
  <?php print @$this->model->code; ?>
</td>
<td>
  <?php print @$this->model->fromName; ?>
</td>
<td>
  <?php 
  $date = new DateTime();
  $date->setTimestamp(@$this->model->departure / 1000);
  print $date->format('d.m.Y');
  ?>
</td>
<td>
  <?php print @$this->model->toName; ?>
</td>
<td>
  <?php 
  $date = new DateTime();
  $date->setTimestamp(@$this->model->arrival / 1000);
  print $date->format('d.m.Y');
  ?>
</td>
<td>
  <?php print @$this->model->planeName; ?>
</td>
<td>
  <?php print @$this->model->capacity; ?>
</td>
<td>
  <a href="<?php print @$this->ROOTPATH; ?>evidence/edit/flight/<?php print @$this->model->id; ?>">Editovat</a>
</td>
<td>
  <a href="<?php print @$this->ROOTPATH; ?>evidence/del/flight/<?php print @$this->model->id; ?>">Smazat</a>
</td>
