<table>
  <tr>
    <th>
      <label for="lname">
        Město:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="hidden" maxlength="255" name="id" id="lid" size="5" value="<?php print @$this->model["id"]; ?>" readonly />
      <input type="text" maxlength="255" name="name" id="lname" size="60" value="<?php print @$this->model["name"]; ?>" />
    </td>
  </tr>
  <tr>
    <th>
      <label for="lidCountry">
        Země:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="hidden" maxlength="255" name="idCountry" id="lidCountry" size="5" value="<?php print @$this->model["idCountry"]; ?>" readonly />
        <select onChange="jsFunction()" name="idCountryCB" id="lidCountryCB" size="1">
        </select>
    </td>
  </tr>
  
  <tr>
    <th></th>
    <td>
      <input type="submit" name="send" value="Uložit" />
    </td>
  </tr>
</table>

<script type="text/javascript" charset="utf-8">
  function jsFunction(){
  var myselect = document.getElementById("lidCountryCB");
  $('#lidCountry').val(myselect.options[myselect.selectedIndex].value);
  }

  $(document).ready(function(){
  $.getJSON('<?php print @$this->APPURL; ?>/get/country', function(data){
  // Fill ComboBox
  $.each(data, function(i, item){
  $('#lidCountryCB').append($('<option>', {
    value: item.id,
    text : item.name
    }));
    });

    // Set actual value
    $('#lidCountryCB').val(<?php print @$this->model["idCountry"]; ?>);
    });
    });

  </script>