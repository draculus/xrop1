<table>
  <tr>
    <th>
      <label for="lid">
        Zkratka:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="text" maxlength="255" name="id" id="lid" size="5" value="<?php print @$this->model["id"]; ?>" />
    </td>
  </tr>
  <tr>
    <th>
      <label for="lname">
        Letiště:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="text" maxlength="255" name="name" id="lname" size="60" value="<?php print @$this->model["name"]; ?>" />
    </td>
  </tr>
  <tr>
    <th>
      <label for="lidCity">
        Město:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="hidden" maxlength="255" name="idCity" id="lidCity" size="5" value="<?php print @$this->model["idCity"]; ?>" readonly/>
      <select onChange="jsFunction()" name="idCityCB" id="lidCityCB" size="1">
      </select>
    </td>
  </tr>
  <tr>
    <th></th>
    <td>
      <input type="submit" name="send" value="Uložit" />
    </td>
  </tr>
</table>

<script type="text/javascript" charset="utf-8">
function jsFunction(){
  var myselect = document.getElementById("lidCityCB");
  $('#lidCity').val(myselect.options[myselect.selectedIndex].value);
}

$(document).ready(function(){
  $.getJSON('<?php print @$this->APPURL; ?>/get/city', function(data){
    // Fill ComboBox
	$.each(data, function(i, item){	  
	  $('#lidCityCB').append($('<option>', { 
        value: item.id,
        text : item.name 
      }));
    });

	// Set actual value
	$('#lidCityCB').val(<?php print @$this->model["idCity"]; ?>);
  });
});

</script>