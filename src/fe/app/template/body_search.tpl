<tr>
  <td>
    <?php print @$this->model->id; ?>
  </td>
  <td>
    <?php print @$this->model->code; ?>
  </td>
  <td>
    <?php print @$this->model->airportFrom; ?>
  </td>
  <td>
    <?php print @$this->model->airportTo; ?>
  </td>
  <td>
    <?php print @$this->model->idPlane; ?>
  </td>
  <td>
    <?php
    $date = new DateTime();
    $date->setTimestamp(@$this->model->departure / 1000);
    print $date->format('d.m.Y H:i:s'); 
    ?>
  </td>
  <td>
    <?php
    $date = new DateTime();
    $date->setTimestamp(@$this->model->arrival / 1000);
    print $date->format('d.m.Y H:i:s'); 
    ?>
  </td>
  <td>
    <?php print @$this->model->capacity; ?>
  </td>
  <td>
    <a href="<?php print @$this->ROOTPATH; ?>services/reserve/<?php print @$this->model->id; ?>">Rezervovat </a>
  </td>
</tr>
