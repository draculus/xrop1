<p>Pro vyhledání rezervace letu Vám pomůže tento průvodce. </p>
<table>
  <tr>
    <th>
      <label for="aldate">
        Jméno:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="text" name="fdate" id="aldate" size="60" value="" />
    </td>
  </tr>

  <tr>
    <th>
      <label for="bldate">
        Příjmení:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="text" name="fdate" id="bldate" size="60" value="" />
    </td>
  </tr>

  <tr>
    <th>
      <label for="ldate">
        Datum odletu:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <div id="datepicker"></div>
      <input type="hidden" name="fdate" id="ldate" size="60" value="" />
    </td>
  </tr>
    
  <tr>
    <th>
      <label for="lidCityFrom">
        Odlet z:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="hidden" maxlength="255" name="idCityFrom" id="lidCityFrom" size="5" value="" readonly />
      <select onChange="jsFunctionFrom()" name="idCityFromCB" id="lidCityFromCB" size="1">
        <option> (vyberte jednu možnost) </option>
      </select>
    </td>
  </tr>

  <tr>
    <th>
      <label for="lidCityTo">
        Přílet do:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="hidden" maxlength="255" name="idCityTo" id="lidCityTo" size="5" value="" readonly="" />
      <select onChange="jsFunctionTo()" name="idCityToCB" id="lidCityToCB" size="1">
        <option> (vyberte jednu možnost) </option>
      </select>
    </td>
  </tr>  
  
  <tr>
    <th></th>
    <td>
      <input type="submit" name="send" value="Odeslat" />
    </td>
  </tr>
</table>

<script type="text/javascript" charset="utf-8">
  $(function() {
  $("#datepicker").datepicker({ inline : true,
  altField : '#ldate'
  });
  });

  function jsFunctionFrom(){
  var myselect = document.getElementById("lidCityFromCB");
  $('#lidCityFrom').val(myselect.options[myselect.selectedIndex].value);
  }

  function jsFunctionTo(){
  var myselect = document.getElementById("lidCityToCB");
  $('#lidCityTo').val(myselect.options[myselect.selectedIndex].value);
  }

  $(document).ready(function(){
  $.getJSON('<?php print @$this->APPURL; ?>/get/airport', function(data){
    // Fill ComboBox
	$.each(data, function(i, item){	  
	  $('#lidCityFromCB').append($('<option>', { 
        value: item.id,
        text : item.name 
      }));
    $('#lidCityToCB').append($('<option>', { 
        value: item.id,
        text : item.name 
      }));      
    });

	// Set actual value
  $('#lidCityFromCB').val(<?php print @$this->model["idCityFrom"]; ?>);
	$('#lidCityToCB').val(<?php print @$this->model["idCityTo"]; ?>);
  });
});

</script>