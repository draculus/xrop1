<table>
  <tr>
    <th>
      <label for="llogin">
        Přihlašovací jméno:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="text" maxlength="255" name="login" id="llogin" size="60" value="" />
    </td>
  </tr>
  <tr>
    <th>
      <label for="lpassword">
        Heslo:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="password" maxlength="255" name="password" id="lpassword" size="60" value="" />
    </td>
  </tr>

  <tr>
    <th></th>
    <td>
      <input type="submit" name="send" value="Odeslat" />
    </td>
  </tr>
</table>
