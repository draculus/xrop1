        <ul id="menu">
            <li><a href="#" title="System">System</a>
                <ul>
                  <?php 
			            if(@$this->USERID=="-1")
			            { ?>
                    <li class="last"><a href="<?php print @$this->ROOTPATH; ?>uac/login" title="Přihlášení">Přihlášení</a></li>
                  <?php } else { ?>
                    <li><a href="<?php print @$this->ROOTPATH; ?>uac/regme" title="Registrace">Registrace</a></li>
                    <li class="last"><a href="<?php print @$this->ROOTPATH; ?>uac/logout" title="Odhlášení">Odhlášení</a></li>
                  <?php } ?>
                </ul>
            </li>
          <?php 
			            if(@$this->USERID!="-1")
			            { ?>
            <li><a href="#" title="Služby">Služby</a>
                <ul>
                    <li class="last"><a href="<?php print @$this->ROOTPATH; ?>services/reserve_flight" title="Rezervace letu">Rezervace letu</a></li>
                    <!-- <li><a href="<?php print @$this->ROOTPATH; ?>services/search_flights" title="Vyhledávání rezervací">Vyhledávání rezervací</a></li> -->
                </ul>
            </li>
            <li><a href="#" title="Přeprava">Přeprava</a>
                <ul>                                    
                  <!-- <li><a href="<?php print @$this->ROOTPATH; ?>evidence/browse/order" title="Objednávky">Objednávky</a></li> -->
                  <li><a href="<?php print @$this->ROOTPATH; ?>evidence/browse/reservation" title="Rezervace">Rezervace</a></li>
                  <li><a href="<?php print @$this->ROOTPATH; ?>evidence/browse/flight" title="Lety">Lety</a></li>
                  <li><a href="<?php print @$this->ROOTPATH; ?>evidence/browse/airport" title="Letiště">Letiště</a></li>
                  <li><a href="<?php print @$this->ROOTPATH; ?>evidence/browse/plane" title="Letadla">Letadla</a></li>
                  <li class="last"><a href="<?php print @$this->ROOTPATH; ?>evidence/browse/airlines" title="Společnosti">Společnosti</a></li>  
                </ul>
            </li> 
            <li><a href="#" title="Číselníky">Číselníky</a>
                <ul>                  
                  <li><a href="<?php print @$this->ROOTPATH; ?>evidence/browse/country" title="Země">Země</a></li>
                  <li><a href="<?php print @$this->ROOTPATH; ?>evidence/browse/city" title="Města">Města</a></li>
                  <li class="last"><a href="<?php print @$this->ROOTPATH; ?>evidence/browse/currency" title="Měny">Měny</a></li>
                </ul>
            </li>            
            <li><a href="#" title="Nastavení">Nastavení</a>
              <ul>
                <li><a href="<?php print @$this->ROOTPATH; ?>evidence/browse/person" title="Uživatelé">Uživatelé</a></li>
                <li class="last"><a href="<?php print @$this->ROOTPATH; ?>evidence/browse/personrole" title="Role">Role</a></li>
              </ul>
            </li>
          <?php } ?>
            <li class="last"><a href="<?php print @$this->ROOTPATH; ?>main/index" title="O aplikaci...">O aplikaci...</a></li>
        </ul>

<script type="text/javascript" charset="utf-8">
  $("#menu").menu({position: {at: "left bottom"}});
</script>