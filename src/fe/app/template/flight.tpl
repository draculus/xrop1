<table>
  <tr>
    <th>
      <label for="lcode">
        Kód:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="hidden" maxlength="255" name="id" id="lid" size="5" value="<?php print @$this->model["id"]; ?>" readonly />
      <input type="text" maxlength="255" name="code" id="lcode" size="60" value="<?php print @$this->model["code"]; ?>" />
    </td>
  </tr>
  
  <tr>
    <th>
      <label for="lairportFrom">
        Odlet z letiště:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="hidden" maxlength="255" name="airportFrom" id="lairportFrom" size="5" value="<?php print @$this->model["airportFrom"]; ?>" readonly />
      <select onChange="jsFunctionAF()" name="airportFromCB" id="lairportFromCB" size="1">
      </select>
    </td>
  </tr>

  <tr>
    <th>
      <label for="ldeparture">
        Datum odletu:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <div id="datepicker1"></div>
      <input type="text" maxlength="255" name="departure" id="ldeparture" size="60" value="<?php print @$this->model["departure"]; ?>" />
    </td>
  </tr>

  <tr>
    <th>
      <label for="lairportTo">
        Přílet na letiště:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="hidden" maxlength="255" name="airportTo" id="lairportTo" size="5" value="<?php print @$this->model["airportTo"]; ?>" readonly />
      <select onChange="jsFunctionAT()" name="airportToCB" id="lairportToCB" size="1">
      </select>
    </td>
  </tr>
  
  <tr>
    <th>
      <label for="larrival">
        Datum příletu:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <div id="datepicker2"></div>
      <input type="text" maxlength="255" name="arrival" id="larrival" size="60" value="<?php print @$this->model["arrival"]; ?>" />
    </td>
  </tr>  

  <tr>
    <th>
      <label for="lidPlane">
        Letadlo:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="hidden" maxlength="255" name="idPlane" id="lidPlane" size="5" value="<?php print @$this->model["idPlane"]; ?>" readonly />
      <select onChange="jsFunction()" name="idPlaneCB" id="lidPlaneCB" size="1">
      </select>
    </td>
  </tr>

  <tr>
    <th>
      <label for="lcapacity">
        Počet míst:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="text" maxlength="255" name="capacity" id="lcapacity" size="60" value="<?php print @$this->model["capacity"]; ?>" />
    </td>
  </tr>
  
  <tr>
    <th></th>
    <td>
      <input type="submit" name="send" value="Uložit" />
    </td>
  </tr>
</table>

<script type="text/javascript" charset="utf-8">
  function jsFunctionAF(){
  var myselect = document.getElementById("lairportFromCB");
  $('#lairportFrom').val(myselect.options[myselect.selectedIndex].value);
  }

  function jsFunctionAT(){
  var myselect = document.getElementById("lairportToCB");
  $('#lairportTo').val(myselect.options[myselect.selectedIndex].value);
  }

  function jsFunction(){
  var myselect = document.getElementById("lidPlaneCB");
  $('#lidPlane').val(myselect.options[myselect.selectedIndex].value);
  }

  $(document).ready(function(){
  $("#datepicker1").datepicker({ inline : true, altField : '#ldeparture'});
  $('#datepicker1').datepicker('setDate', new Date(<?php
    $date = new DateTime('now');
    if (@$this->model["departure"] != "")
    {
      $date->setTimestamp(@$this->model["departure"] / 1000);
    }
    $d = $date->format('d');
    $m = $date->format('m');
    $Y = $date->format('Y');
    
    // JavaScript ma mesice od 0
    $m = $m -1;
    
    echo $Y.",".$m.",".$d;
  ?>));

  $("#datepicker2").datepicker({ inline : true, altField : '#larrival'});
  $('#datepicker2').datepicker('setDate', new Date(<?php
    $date = new DateTime('now');
    if (@$this->model["arrival"] != "")
    {
      $date->setTimestamp(@$this->model["arrival"] / 1000);
    }
    $d = $date->format('d');
    $m = $date->format('m');
    $Y = $date->format('Y');
    
    // JavaScript ma mesice od 0
    $m = $m -1;
    
    echo $Y.",".$m.",".$d;
  ?>));
  
  $.getJSON('<?php print @$this->APPURL; ?>/get/plane', function(data){
  // Fill ComboBox
  $.each(data, function(i, item){
  $('#lidPlaneCB').append($('<option>', { 
        value: item.id,
        text : item.name 
      }));
    });

	// Set actual value
	$('#lidPlaneCB').val(<?php print @$this->model["idPlane"]; ?>);
  });
  
  $.getJSON('<?php print @$this->APPURL; ?>/get/airport', function(data){
  // Fill ComboBox
  $.each(data, function(i, item){
  $('#lairportFromCB').append($('<option>', { 
        value: item.id,
        text : item.name 
      }));
  $('#lairportToCB').append($('<option>', { 
        value: item.id,
        text : item.name 
      }));
    });

	// Set actual value
	$('#lairportFromCB').val('<?php print @$this->model["airportFrom"]; ?>');
  $('#lairportToCB').val('<?php print @$this->model["airportTo"]; ?>');
  });
});

</script>