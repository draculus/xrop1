	
	<table>
                <tr>
                    <th><label for="lfirstname">Jméno:<span title="Toto pole je vyžadováno.">*</span></label></th>
                    <td>
						<input type="hidden" maxlength="255" name="id" id="lid" size="5" value="<?php print @$this->model["id"]; ?>" readonly />
						<input type="text" maxlength="255" name="firstname" id="lfirstname" size="60" value="<?php print @$this->model["firstname"]; ?>" /></td>
                </tr>
				<tr>
                    <th><label for="lsurname">Příjmení:<span title="Toto pole je vyžadováno.">*</span></label></th>
                    <td><input type="text" maxlength="255" name="surname" id="lsurname" size="60" value="<?php print @$this->model["surname"]; ?>" /></td>
                </tr>
                <tr>
                    <th><label for="lstreet">Ulice:<span title="Toto pole je vyžadováno.">*</span></label></th>
                    <td><input type="text" maxlength="255" name="street" id="lstreet" size="60" value="<?php print @$this->model["street"]; ?>" /></td>
                </tr>
				<tr>
                    <th><label for="lidCity">Město:<span title="Toto pole je vyžadováno.">*</span></label></th>
                    <td>
						<input type="hidden" maxlength="255" name="idCity" id="lidCity" size="5" value="<?php print @$this->model["idCity"]; ?>" readonly/>
						<select onChange="jsFunction()" name="idCityCB" id="lidCityCB" size="1">
						</select>
					</td>
                </tr>
                <tr>
                    <th></th>
                    <td><input type="submit" name="send" value="Uložit" /></td>
                </tr>
    </table>

<script type="text/javascript" charset="utf-8">
function jsFunction(){
  var myselect = document.getElementById("lidCityCB");
  $('#lidCity').val(myselect.options[myselect.selectedIndex].value);
}

$(document).ready(function(){
  $.getJSON('<?php print @$this->APPURL; ?>/get/city', function(data){
    // Fill ComboBox
	$.each(data, function(i, item){	  
	  $('#lidCityCB').append($('<option>', { 
        value: item.id,
        text : item.name 
      }));
    });

	// Set actual value
	$('#lidCityCB').val(<?php print @$this->model["idCity"]; ?>);
  });
});

</script>