<table>
  <tr>
    <th>
      <label for="lidPerson">
        Uživatel:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="hidden" maxlength="255" name="id" id="lid" size="5" value="<?php print @$this->model["id"]; ?>" readonly />
      <input type="hidden" maxlength="255" name="idPerson" id="lidPerson" size="5" value="<?php print @$this->model["idPerson"]; ?>" readonly />
      <select onChange="jsSetPerson()" name="idPersonCB" id="lidPersonCB" size="1">
      </select>
    </td>
  </tr>

  <tr>
    <th>
      <label for="lidPersonRole">
        Role:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="hidden" maxlength="255" name="idPersonRole" id="lidPersonRole" size="5" value="<?php print @$this->model["idPersonRole"]; ?>" readonly />
      <select onChange="jsSetPersonRole()" name="idPersonRoleCB" id="lidPersonRoleCB" size="1">
      </select>
    </td>
  </tr>

  <tr>
    <th>
      <label for="lidFlight">
        Let:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="hidden" maxlength="255" name="idFlight" id="lidFlight" size="5" value="<?php print @$this->model["idFlight"]; ?>" readonly />
      <select onChange="jsSetFlight()" name="idFlightCB" id="lidFlightCB" size="1">
      </select>
    </td>
  </tr>

  <tr>
    <th>
      <label for="lidCurrency">
        Měna:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="hidden" maxlength="255" name="idCurrency" id="lidCurrency" size="5" value="<?php print @$this->model["idCurrency"]; ?>" readonly />
      <select onChange="jsSetCurrency()" name="idCurrencyCB" id="lidCurrencyCB" size="1">
      </select>
    </td>
  </tr>

  <tr>
    <th>
      <label for="lidOrder">
        Objednávka:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="hidden" maxlength="255" name="idOrder" id="lidOrder" size="5" value="<?php print @$this->model["idOrder"]; ?>" readonly />
      <select onChange="jsSetOrder()" name="idOrderCB" id="lidOrderCB" size="1">
      </select>
    </td>
  </tr>

  <tr>
    <th>
      <label for="lprice">
        Cena:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="text" maxlength="255" name="price" id="lprice" size="60" value="<?php print @$this->model["price"]; ?>" />
    </td>
  </tr>
  
  <tr>
    <th></th>
    <td>
      <input type="submit" name="send" value="Uložit" />
    </td>
  </tr>
</table>

<script type="text/javascript" charset="utf-8">

  function jsSetPerson(){
    var myselect = document.getElementById("lidPersonCB");
    $('#lidPerson').val(myselect.options[myselect.selectedIndex].value);
  }
  
  function jsSetPersonRole(){
    var myselect = document.getElementById("lidPersonRoleCB");
    $('#lidPersonRole').val(myselect.options[myselect.selectedIndex].value);
  }
  
  function jsSetFlight(){
    var myselect = document.getElementById("lidFlightCB");
    $('#lidFlight').val(myselect.options[myselect.selectedIndex].value);
  }
  
  function jsSetCurrency(){
    var myselect = document.getElementById("lidCurrencyCB");
    $('#lidCurrency').val(myselect.options[myselect.selectedIndex].value);
  }
  
  function jsSetOrder(){
    var myselect = document.getElementById("lidOrderCB");
    $('#lidOrder').val(myselect.options[myselect.selectedIndex].value);
  }
  
  function fillPerson() {
    // Get from BE
    $.getJSON('<?php print @$this->APPURL; ?>/get/person', function(data) {
  
      // Fill ComboBox
      $.each(data, function(i, item) {
        $('#lidPersonCB').append($('<option>', { value: item.id, text : item.surname+' '+item.firstname }));
      });

      // Set actual value
      $('#lidPersonCB').val(<?php print @$this->model["idPerson"]; ?>);
    });
  }

  function fillPersonRole() {
    // Get from BE
    $.getJSON('<?php print @$this->APPURL; ?>/get/personrole', function(data) {
  
      // Fill ComboBox
      $.each(data, function(i, item) {
        $('#lidPersonRoleCB').append($('<option>', { value: item.id, text : item.name }));
      });

      // Set actual value
      $('#lidPersonRoleCB').val(<?php print @$this->model["idPersonRole"]; ?>);
    });
  }
  
  function fillFlight() {
    // Get from BE
    $.getJSON('<?php print @$this->APPURL; ?>/get/flight', function(data) {
  
      // Fill ComboBox
      $.each(data, function(i, item) {
        $('#lidFlightCB').append($('<option>', { value: item.id, text : item.code }));
      });

      // Set actual value
      $('#lidFlightCB').val(<?php print @$this->model["idFlight"]; ?>);
    });
  }
  
  function fillCurrency() {
    // Get from BE
    $.getJSON('<?php print @$this->APPURL; ?>/get/currency', function(data) {
  
      // Fill ComboBox
      $.each(data, function(i, item) {
        $('#lidCurrencyCB').append($('<option>', { value: item.id, text : item.name }));
      });

      // Set actual value
      $('#lidCurrencyCB').val('<?php print @$this->model["idCurrency"]; ?>');
    });
  }
  
  function fillOrder() {
    // Get from BE
    $.getJSON('<?php print @$this->APPURL; ?>/get/order', function(data) {
  
      // Fill ComboBox
      $.each(data, function(i, item) {
        $('#lidOrderCB').append($('<option>', { value: item.id, text : item.date }));
      });

      // Set actual value
      $('#lidOrderCB').val(<?php print @$this->model["idOrder"]; ?>);
    });
  }
  
  $(document).ready(function(){
    fillPerson();
    fillPersonRole();
    fillFlight();
    fillCurrency();
    fillOrder();
    
  });

</script>