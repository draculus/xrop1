	<table>
				<tr>
                    <th><label for="lname">Označení:<span title="Toto pole je vyžadováno.">*</span></label></th>
                    <td>
						<input type="hidden" maxlength="255" name="id" id="lid" size="5" value="<?php print @$this->model["id"]; ?>" readonly />
						<input type="text" maxlength="255" name="name" id="lname" size="55" value="<?php print @$this->model["name"]; ?>" />
					</td>
                </tr>
				<tr>
                    <th><label for="idAirlines">Společnost:<span title="Toto pole je vyžadováno.">*</span></label></th>
                    <td>
					    <input type="hidden" maxlength="255" name="idAirlines" id="idAirlines" size="5" value="<?php print @$this->model["idAirlines"]; ?>" readonly />
						<select onChange="jsFunction()" name="idAirlinesCB" id="idAirlinesCB" size="1">
						</select>						
					</td>
                </tr>				
                <tr>
                    <th><label for="lseats">Míst:<span title="Toto pole je vyžadováno.">*</span></label></th>
                    <td><input type="text" maxlength="255" name="seats" id="lseats" size="10" value="<?php print @$this->model["seats"]; ?>" /></td>
                </tr>
                <tr>
					<th></th>
                    <td><input type="submit" name="send" value="Uložit" /></td>
                </tr>
    </table>

<script type="text/javascript" charset="utf-8">
function jsFunction(){
  var myselect = document.getElementById("idAirlinesCB");
  $('#idAirlines').val(myselect.options[myselect.selectedIndex].value);
}

$(document).ready(function(){
  $.getJSON('<?php print @$this->APPURL; ?>/get/airlines', function(data){
    // Fill ComboBox
	$.each(data, function(i, item){	  
	  $('#idAirlinesCB').append($('<option>', { 
        value: item.id,
        text : item.name 
      }));
    });

	// Set actual value
	$('#idAirlinesCB').val(<?php print @$this->model["idAirlines"]; ?>);
  });
});

</script>