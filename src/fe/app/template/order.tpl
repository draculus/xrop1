<table>
  <tr>
    <th>
      <label for="lname">
        Datum objednávky:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="hidden" maxlength="255" name="id" id="lid" size="5" value="<?php print @$this->model["id"]; ?>" readonly />
      <div id="datepicker"></div>
      <input type="hidden" maxlength="255" name="date" id="lname" size="60" value="<?php print @$this->model["date"]; ?>" />
    </td>
  </tr>
  
  <tr>
    <th></th>
    <td>
      <input type="submit" name="send" value="Odeslat" />
    </td>
  </tr>
</table>

<script type="text/javascript" charset="utf-8">
  $(function() {
  $("#datepicker").datepicker({ inline : true, altField : '#lname'});
  $('#datepicker').datepicker('setDate', new Date(<?php
    $date = new DateTime();
    $date->setTimestamp(@$this->model["date"] / 1000);
    $d = $date->format('d');
    $m = $date->format('m');
    $Y = $date->format('Y');
    
    // JavaScript ma mesice od 0
    $m = $m -1;
    
    echo $Y.",".$m.",".$d;
  ?>));
  });

</script>