<head>
    <meta charset="utf-8" />
    <title><?php print @$this->title; ?></title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width; initial-scale=1.0" />
    <link href="<?php print @$this->CSSPATH; ?>html5reset.css" type="text/css" rel="stylesheet" />    
    <link href="<?php print @$this->CSSPATH; ?>jquery-ui.css" type="text/css" rel="stylesheet" />
    <link href="<?php print @$this->CSSPATH; ?>style.css" type="text/css" rel="stylesheet" />
    <script src="<?php print @$this->JSPATH; ?>jquery-1.9.1.js"></script>
    <script src="<?php print @$this->JSPATH; ?>jquery-ui.js"></script>       
    <!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	  <![endif]-->      
</head>

