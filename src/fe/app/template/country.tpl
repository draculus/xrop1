<table>
  <tr>
    <th>
      <label for="lcode">
        Kód:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="hidden" maxlength="255" name="id" id="lid" size="5" value="<?php print @$this->model["id"]; ?>" readonly />
      <input type="text" maxlength="255" name="code" id="lcode" size="60" value="<?php print @$this->model["code"]; ?>" />
    </td>
  </tr>
  <tr>
    <th>
      <label for="lname">
        Stát:<span title="Toto pole je vyžadováno.">*</span>
      </label>
    </th>
    <td>
      <input type="text" maxlength="255" name="name" id="lname" size="60" value="<?php print @$this->model["name"]; ?>" />
    </td>
  </tr>

  <tr>
    <th></th>
    <td>
      <input type="submit" name="send" value="Uložit" />
    </td>
  </tr>
</table>