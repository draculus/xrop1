<?php
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) 
{
  exit("You can't access this file directly !");
}

class Module
{
	public $core;
	
	public function __construct(&$core)
	{
		$this->core = $core;
	}
	
	function LoginDialog()
	{
		$res = "<img src=\"application/images/login.png\" alt=\"Přihlásit se\" title=\"Přihlásit se\" /><h2>Přihlášení...</h2>\n";				
		$res .= "<div id=\"form\"><form method=\"post\" action=\"index.php\">";
		$res .= "<table><tr><td>Login:</td><td><input type=\"text\" name=\"login\" maxlength=\"20\" /></td></tr>";
		$res .= "<tr><td>Heslo:</td><td><input type=\"password\" name=\"password\" maxlength=\"20\" /></td></tr>";
		$res .= "<tr><td></td><td class=\"right\"><input type=\"submit\" value=\"Přihlásit se\" class=\"tlacitko\" /></td></tr></table></form></div>";
		$res .= "<p>Pro práci v systému je třeba se přihlásit.</p>\n";
		return $res;
	}
	
	public function Render()
	{							
		if ($this->core->security->GetUserId()==-1)
		{	
			$res = "<div>\n";				
			$Login = $this -> core->security -> GetPost('login');			
			$Passwd = $this -> core->security -> GetPost('password');
			if($Login[0] == 1 && $Login[1] != '' && $Passwd[0] == 1 && $Passwd[1] != '')
			{
				if( $this -> core->security -> Login($Login[1], $Passwd[1]) == 1 )
				{					
					$res .= $this -> core->message -> DisplayConfirm("<h2>Byl jste úspěšně přihlášen.</h2><div id=\"form\"><table><form method=\"post\" action=\"?\"><tr><td class=\"right\"><input type=\"submit\" value=\"OK\" class=\"tlacitko\" /></td></tr></form></table></div>");
				}
				else
				{
					$res .= $this -> core->message -> DisplayError('Zadali jste chybné jméno nebo heslo.<br />');
					$res .= $this->LoginDialog();
				}
			}
			else
			{
				//$res .= $this -> message -> DisplayQuestion("<h2>Pøihlásit se</h2><div id=\"form\"><table><form method=\"post\" action=\"?id=".$this -> Objects['Content'] -> id."\"><tr><td><input type=\"text\" name=\"login\" maxlength=\"20\" /></td></tr><tr><td><input type=\"password\" name=\"passwd\" maxlength=\"20\" /></td></tr><tr><td class=\"right\"><input type=\"submit\" value=\"Pøihlásit se\" class=\"tlacitko\" /></td></tr></form></table></div>");
				$res .= $this->LoginDialog();
			}
			$res .= "</div>\n";			
		}
		else
		{			
			$this->core->security->Logout();			
			$res = $this -> core->message -> DisplayConfirm("<h2>Byl jste úspěšně odhlášen.</h2><div id=\"form\"><table><form method=\"post\" action=\"?\"><tr><td class=\"right\"><input type=\"submit\" value=\"OK\" class=\"tlacitko\" /></td></tr></form></table></div>");
		}
		
		return $res;
	}
}

?>