<?php
class Airport
{
  var $id = "";
  var $name = "";
  var $idCity = "";
  
  public static function GetJSONFromPOST()
  {
	if (!empty($_POST['id']))
	{
		$rec = array(
		'id'=>$_POST['id'],
		'name'=>$_POST['name'],
		'idCity'=>$_POST['idCity']);
	}
	else
	{
		$rec = array(
		'id'=>null,
		'name'=>$_POST['name'],
		'idCity'=>$_POST['idCity']);
	}
		
	return json_encode($rec);
  }
}
?>
