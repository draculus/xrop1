<?php
class Person
{
  var $id = "";
  var $firstname = "";
  var $surname = "";
  var $street = "";
  var $idCity = "";
  
  public static function GetJSONFromPOST()
  {
	if (!empty($_POST['id']))
	{
		$rec = array(
		'id'=>$_POST['id'],
		'firstname'=>$_POST['firstname'],
		'surname'=>$_POST['surname'],    
		'street'=>$_POST['street'],
		'idCity'=>$_POST['idCity']);
	}
	else
	{
		$rec = array(
		'id'=>null,
		'firstname'=>$_POST['firstname'],
		'surname'=>$_POST['surname'],    
		'street'=>$_POST['street'],
		'idCity'=>$_POST['idCity']);
	}
		
	return json_encode($rec);
  }
}
?>
