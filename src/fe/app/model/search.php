<?php
class Search
{
  var $fdate = "";
  var $idCityFrom = "";
  var $idCityTo = "";
  
  public static function GetArrayFromPOST()
  {
    $rec = array(
    'fdate'=>$_POST['fdate'],
    'idCityFrom'=>$_POST['idCityFrom'],
    'idCityTo'=>$_POST['idCityTo']);
    
    return $rec;
  }
  
  public static function GetJSONFromPOST()
  {
    return json_encode(Search::GetArrayFromPOST());
  }
  
}
?>
