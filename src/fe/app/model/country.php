<?php
class Country
{
  var $id = "";
  var $name = "";
  var $code = "";
  
  public static function GetJSONFromPOST()
  {
	if (!empty($_POST['id']))
	{
		$rec = array(
		'id'=>$_POST['id'],
		'name'=>$_POST['name'],
		'code'=>$_POST['code']);
	}
	else
	{
		$rec = array(
		'id'=>null,
		'name'=>$_POST['name'],
		'code'=>$_POST['code']);
	}
		
	return json_encode($rec);
  }
}
?>
