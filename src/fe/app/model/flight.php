<?php
class Flight
{
  var $id = "";
  var $code = "";
  var $airportFrom = "";
  var $airportTo = "";
  var $idPlane = "";
  var $departure = "";
  var $arrival = "";  
  var $capacity = "";  
  
  public static function GetJSONFromPOST()
  {    
    $date = DateTime::createFromFormat('m/d/Y H:i:s', $_POST["departure"]." 00:00:00");
    $departure = $date->format('Y-m-d');
    
    $date = DateTime::createFromFormat('m/d/Y H:i:s', $_POST["arrival"]." 00:00:00");
    $arrival = $date->format('Y-m-d');
  
    if (!empty($_POST['id']))
	{

        
		$rec = array(
		'id'=>$_POST['id'],
		'code'=>$_POST['code'],
        'airportFrom'=>$_POST['airportFrom'],
        'airportTo'=>$_POST['airportTo'],
        'idPlane'=>$_POST['idPlane'],
        'departure'=>$departure,
        'arrival'=>$arrival,
		'capacity'=>$_POST['capacity']);
        
        var_dump($departure);
	}
	else
	{
		$rec = array(
		'id'=>null,
		'code'=>$_POST['code'],
        'airportFrom'=>$_POST['airportFrom'],
        'airportTo'=>$_POST['airportTo'],
        'idPlane'=>$_POST['idPlane'],
        'departure'=>$departure,
        'arrival'=>$arrival,
		'capacity'=>$_POST['capacity']);
	}
		
	return json_encode($rec);
  }
}
?>
