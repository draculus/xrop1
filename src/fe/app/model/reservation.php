<?php
class Reservation
{
  var $id = "";
  var $idPerson = "";
  var $idPersonRole = "";
  var $idFlight = "";
  var $idCurrency = "";
  var $idOrder = "";
  var $price = "";  
  
  public static function GetArrayFromPOST()
  {
    if (!empty($_POST['id']))
	{
		$rec = array(
		'id'=>$_POST['id'],
		'idPerson'=>$_POST['idPerson'],
        'idPersonRole'=>$_POST['idPersonRole'],
        'idFlight'=>$_POST['idFlight'],
        'idCurrency'=>$_POST['idCurrency'],
        'idOrder'=>$_POST['idOrder'],
		'price'=>$_POST['price']);
	}
	else
	{
		$rec = array(
		'id'=>null,
		'idPerson'=>$_POST['idPerson'],
        'idPersonRole'=>$_POST['idPersonRole'],
        'idFlight'=>$_POST['idFlight'],
        'idCurrency'=>$_POST['idCurrency'],
        'idOrder'=>$_POST['idOrder'],
		'price'=>$_POST['price']);
	}
    
    return $rec;
  }
  
  public static function GetJSONFromPOST()
  {
    return json_encode(Reservation::GetArrayFromPOST());
  }
}
?>
