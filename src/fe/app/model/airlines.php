<?php
class Airlines
{
  var $id = "";
  var $name = "";
  var $idCountry = "";
  
  public static function GetJSONFromPOST()
  {
	if (!empty($_POST['id']))
	{
		$rec = array(
		'id'=>$_POST['id'],
		'name'=>$_POST['name'],
		'idCountry'=>$_POST['idCountry']);
	}
	else
	{
		$rec = array(
		'id'=>null,
		'name'=>$_POST['name'],
		'idCountry'=>$_POST['idCountry']);
	}
		
	return json_encode($rec);
  }
}
?>
