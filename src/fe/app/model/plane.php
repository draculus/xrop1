<?php
class Plane
{
  var $id = "";
  var $idAirlines = "";
  var $name = "";
  var $seats = "";
  
  public static function GetJSONFromPOST()
  {
	if (!empty($_POST['id']))
	{
		$rec = array(
		'id'=>$_POST['id'],
		'idAirlines'=>$_POST['idAirlines'],
		'name'=>$_POST['name'],    
		'seats'=>$_POST['seats']);
	}
	else
	{
		$rec = array(
		'id'=>null,
		'idAirlines'=>$_POST['idAirlines'],
		'name'=>$_POST['name'],    
		'seats'=>$_POST['seats']);
	}
		
	return json_encode($rec);
  }
}
?>
