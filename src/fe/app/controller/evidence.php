<?php
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) 
{
  exit("You can't access this file directly !");
}

class evidence extends Controller
{
    public function evidence()
	{		
		// require_once(SYSPATH."lib/jqgrid_dist.php");
	}

	public function Index()
	{		
		return "OK";
	}
	
	public function browse()
	{
		$core = Core::GetInstance();		
		$params = $core->route->GetParameters();
		$fileString = file_get_contents($core->backend_url."/get/".$params[1]."/detlist");	
		
        if ((strlen($fileString) == 0) || (strstr($fileString, "Unknown entity")!==FALSE))
            $fileString = file_get_contents($core->backend_url."/get/".$params[1]);	
    
		$all = json_decode($fileString);
		
        $result = "<div id=\"title\">".$params[1]."</div>";
		$result .= "<a href=\"".ROOTPATH."evidence/add/".$params[1]."\">Přidat záznam</a>";
		
        $result .= "<table>";
		
		// render table header
		$headerfile = APPPATH."template/header_".$params[1].".tpl";
		if (file_exists($headerfile))
		{
			$header = new Template($headerfile);
			$result .= $header->Parse();
		}
		
        $bodyfile = APPPATH."template/body_".$params[1].".tpl";
		$isbodyfile = file_exists($bodyfile);
       
        $odd = false;
        
		// render table data
		foreach ($all as $rec)
		{
            $recarray = get_object_vars($rec);
        
            if ($odd == false)
            {
                $odd = true;
			    $result .= "<tr class=\"oddrow\">";
            }
            else
            {
                $odd = false;
                $result .= "<tr>";
            }
            
            if (!$isbodyfile)
            {
			    foreach ($rec as $field)
			    {			
			    	$result .= "<td>";
			    	$result .= $field;
			    	$result .= "</td>";				
			    }
                $result .= "<td><a href=\"".ROOTPATH."evidence/edit/".$params[1]."/".$recarray["id"]."\">Editovat</a></td><td><a href=\"".ROOTPATH."evidence/del/".$params[1]."/".$recarray["id"]."\">Smazat</a></td>";
            }
            else
            {
                $body = new Template($bodyfile);
                $body->set("model", $rec);
        		$result .= $body->Parse();
            }
			
			$result .= "</tr>";			
		}
		
		$result .= "</table>";
		
		/*$result = "";

		$result .= "<table id=\"list2\"></table>";
		$result .= "<div id=\"pager2\"></div>";
		
		$head = new Template(APPPATH."template/jq.tpl");  
		$head->set("title", $this->title);
		$result .= $head->Parse();*/
		
		/*$g = new jqgrid();
		$grid["caption"] = "My Sample Grid"; // set grid customizable params
		$g->set_options($grid);
		//$g->table = "tags"; // db table for CRUD operations.

		$result .= $g->render("my_grid_1"); // render grid*/
		
		//return nl2br( htmlspecialchars($fileString) );
		return $result;
	}
	
	public function add()
	{
		$core = Core::GetInstance();
		$params = $core->route->GetParameters();
		$result = "";
        
		if ($_SERVER['REQUEST_METHOD'] === 'POST')
		{	// response for form data
			include(APPPATH."model/".$params[1].".php");			
			$data_string = $params[1]::GetJSONFromPOST();			
			
			$ch = curl_init($core->backend_url."/".$params[1]."/add");      

			curl_setopt_array($ch, array(
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $data_string,
			CURLOPT_HEADER => true,
			CURLOPT_HTTPHEADER => array('Content-Type: application/json', 'Content-Length: ' . strlen($data_string)))
			);
			
			$server_output = curl_exec ($ch);
			curl_close ($ch);
			
			if ($server_output == "OK") 
			{ 
				$result .= "Záznam byl přidán.";
			} 
			else 
			{ 
				$result .= "Server odpověděl: ".$server_output;
			}
		}
		else
		{	// show form for fill	
			$src = APPPATH."template/".$params[1].".tpl";
			if (!file_exists($src))
			{
				$result = "Soubor ".$src." neexistuje";
			}
			else
			{
				$head = new Template($src);  
				$result .= "<form action=\"".ROOTPATH."evidence/add/".$params[1]."\" method=\"post\">";
				$result .= $head->Parse();
				$result .= "</form>";
			}
		}
		
		return $result;
	}
	
	public function edit()
	{
		$core = Core::GetInstance();
		$params = $core->route->GetParameters();
        $result = "";
        
		if ($_SERVER['REQUEST_METHOD'] === 'POST')
		{	// response for form data
			include(APPPATH."model/".$params[1].".php");			
			$data_string = $params[1]::GetJSONFromPOST();	
			
			$ch = curl_init($core->backend_url."/".$params[1]."/add");      

			curl_setopt_array($ch, array(
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $data_string,
			CURLOPT_HEADER => true,
			CURLOPT_HTTPHEADER => array('Content-Type: application/json', 'Content-Length: ' . strlen($data_string)))
			);
			
			$server_output = curl_exec ($ch);
			curl_close ($ch);
			
			if ($server_output == "OK") 
			{ 
				$result = "Záznam byl přidán.";
			} 
			else 
			{ 
				$result = "Server odpověděl: ".$server_output;
			}
		}
		else
		{	// show form for fill	
			$src = APPPATH."template/".$params[1].".tpl";
			if (!file_exists($src))
			{
				$result = "Soubor ".$src." neexistuje";
			}
			else
			{
				$head = new Template($src);  
				
				$fileString = file_get_contents($core->backend_url."/get/".$params[1]."/".$params[2]);
				$model = json_decode($fileString,true);	
                
				$head->set("model", $model);
				$result .= "<form action=\"".ROOTPATH."evidence/edit/".$params[1]."\" method=\"post\">";
				$result .= $head->Parse();
				$result .= "</form>";
			}
		}
		
		return $result;
	}
	
	public function del()
	{
		$core = Core::GetInstance();
		$params = $core->route->GetParameters();
		
		/*
		// Template muzeme pouzit pro dotaz: Opravdu chcete smazat ...
		
		$src = APPPATH."template/del_".$params[1].".tpl";
		if (!file_exists($src))
		{
			$result = "Soubor ".$src." neexistuje";
		}
		else
		{
			$head = new Template($src);  
			//$head->set("title", $this->title);
			$result .= $head->Parse();
		}
		*/
		
		$rec = array('id'=>$params[2]);
		$data_string = json_encode($rec);
		
		$ch = curl_init($core->backend_url."/".$params[1]."/delete");      
		
		curl_setopt_array($ch, array(
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $data_string,
			CURLOPT_HEADER => true,
			CURLOPT_HTTPHEADER => array('Content-Type: application/json', 'Content-Length: ' . strlen($data_string)))
		);
		
		$server_output = curl_exec ($ch);
		curl_close ($ch);
		
		if ($server_output == "OK") 
		{ 
			$result = "Záznam byl vymazán.";
		} 
		else 
		{ 
			$result = "Server odpověděl: ".$server_output;
		}
		
		return $result;
	}
}
?>