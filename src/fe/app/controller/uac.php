﻿<?php
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) 
{
  exit("You can't access this file directly !");
}

class uac extends Controller
{
    public function uac()
	{		
	}

	public function Index()
	{		
		return "OK";
	}
	
	public function login()
	{
        $core = Core::GetInstance();
		$params = $core->route->GetParameters();
		$result = "";
        
		if ($_SERVER['REQUEST_METHOD'] === 'POST')
		{	// response for form data
            include(APPPATH."model/user.php");			
			$data_string = User::GetJSONFromPOST();			            
            
			$ch = curl_init($core->backend_url."/login");      

			curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $data_string,
			CURLOPT_HEADER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_HTTPHEADER => array('Content-Type: application/json', 'Content-Length: ' . strlen($data_string)))
			);
			
			$server_output = curl_exec ($ch);
            $info = curl_getinfo($ch);
            $code = $info['http_code'];
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            curl_close ($ch);
            
            $data = substr( $server_output, $header_size );
            $class = json_decode($data, true);
            //$result .= $server_output.json_last_error();
            
            if ($code == 200)
            {
                $core->security->SetUserId( $class['id'] ); // 0
				$core->security->SetGroupId( 1 ); // 0
				$core->security->SetUserName( $class['firstname']." ".$class['surname'] );
                $result .= $this->LoginOk();
            }
            else
            {
                $result .= $this->LoginFailed();
            }
        }
        else
        {
            $result .= $this->LoginDialog();
        }
        
        return $result;
	}
	
    public function logout()
	{
        $core = Core::GetInstance();
		$core->security->Logout();
        
        $result = $this->LogoutDialog();
        
        return $result;
	}
    
	public function regme()
	{
	    $core = Core::GetInstance();
		$params = $core->route->GetParameters();
		$result = "";
	
		if ($_SERVER['REQUEST_METHOD'] === 'POST')
		{	// response for form data
		    //$User = $core->security -> GetPost('user');		
            //$Login = $core->security -> GetPost('login');			
			//$Passwd = $core->security -> GetPost('password');
            //$Home = $core->security -> GetPost('home');
            //$core->security->Register($User, $Login, $Passwd, $Home);
            include(APPPATH."model/user.php");			
			$data_string = User::GetJSONFromPOST();
            
            $ch = curl_init($core->backend_url."/user/add");      

			curl_setopt_array($ch, array(
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $data_string,
			CURLOPT_HEADER => true,
			CURLOPT_HTTPHEADER => array('Content-Type: application/json', 'Content-Length: ' . strlen($data_string)))
			);
			
			$server_output = curl_exec ($ch);
			curl_close ($ch);
			
			if ($server_output == "OK") 
			{ 
				$result = "Záznam byl přidán.";
			} 
			else 
			{ 
				$result = "Server odpověděl: ".$server_output;
			}
        }
        else
        {
    		$result .= $this->RegisterDialog();
        }
        
        return $result;
	}
    
    public function LoginDialog()
    {
        $res = "";
        $src = APPPATH."template/login.tpl";
		if (!file_exists($src))
		{
    	    $res = "Soubor ".$src." neexistuje";
        }
	    else
	    {
            $head = new Template($src);  
            //$head->set("title", $this->title);
            $res .= "<form action=\"".ROOTPATH."uac/login/\" method=\"post\">";
	        $res .= $head->Parse();
		    $res .= "</form>";
        }
        return $res;
    }
    
    public function LoginOk()
    {
        $res = "";
        $src = APPPATH."template/loginok.tpl";
		if (!file_exists($src))
		{
    	    $res = "Soubor ".$src." neexistuje";
        }
	    else
	    {
            $head = new Template($src);  
	        $res .= $head->Parse();
        }
        return $res;
    }
    
    public function LoginFailed()
    {
        $res = "";
        $src = APPPATH."template/loginfailed.tpl";
		if (!file_exists($src))
		{
    	    $res = "Soubor ".$src." neexistuje";
        }
	    else
	    {
            $head = new Template($src);  
	        $res .= $head->Parse();
        }
        return $res;
    }
    
    public function LogoutDialog()
    {
        $res = "";
        $src = APPPATH."template/logout.tpl";
		if (!file_exists($src))
		{
    	    $res = "Soubor ".$src." neexistuje";
        }
	    else
	    {
            $head = new Template($src);  
	        $res .= $head->Parse();
        }
        return $res;
    }
    
    public function RegisterDialog()
    {
        $res = "";
        $src = APPPATH."template/register.tpl";
		if (!file_exists($src))
		{
    	    $res = "Soubor ".$src." neexistuje";
        }
	    else
	    {
            $head = new Template($src);  
            //$head->set("title", $this->title);
            $res .= "<form action=\"".ROOTPATH."uac/regme/\" method=\"post\">";
	        $res .= $head->Parse();
		    $res .= "</form>";
        }
        return $res;
    }
}	
?>