<?php
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) 
{
  exit("You can't access this file directly !");
}

class main extends Controller
{
	public function Index()
	{
		return "<h1>Společnost Poletucha je malá společnost zaměřená na prodej letů firemním i soukromým zákazníkům. Zabývá se zprostředkování leteckých dopravních služeb menším subjektům.</h1>";
	}
}
?>