<?php
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) 
{
  exit("You can't access this file directly !");
}

class services extends Controller
{
    public function services()
	{		
	}

	public function Index()
	{		
		return "OK";
	}
	
	public function reserve_flight()
	{
        $core = Core::GetInstance();
		$params = $core->route->GetParameters();
		$result = "";
        
		if ($_SERVER['REQUEST_METHOD'] === 'POST')
		{	// response for form data
            include(APPPATH."model/search.php");			
			$data_string = Search::GetArrayFromPOST();	
            
            // prepare BE request
            $parameters = "";
            if ($data_string["fdate"] != "")
            {
                if ($parameters != "") 
                    $parameters.= "&";
                //06/01/2013 Y-m-d H:i:s 2013-06-26 10:48:00
                $date = DateTime::createFromFormat('m/d/Y H:i:s', $data_string["fdate"]." 00:00:00");
                $ts = $date->format('Y-m-d%20H:i:s');
                $parameters .= "departure=".$ts;
            }
            
            if ($data_string["idCityFrom"] != "")
            {
                if ($parameters != "") 
                    $parameters.= "&";
                $parameters .= "airportFrom=".$data_string["idCityFrom"];
            }    
                
            if ($data_string["idCityTo"] != "")
            {
                if ($parameters != "") 
                    $parameters.= "&";
                $parameters .= "airportTo=".$data_string["idCityTo"];
            }
            
            // get BE response
            $fileString = file_get_contents($core->backend_url."/search/flight?".$parameters);
            $flights = json_decode($fileString);
            
            $result .= "<p>Byli nalezeny tyto lety:</p>";
            $result .= "<table>";
		
        	// render table header
        	$headerfile = APPPATH."template/header_search.tpl";
        	if (file_exists($headerfile))
        	{
        		$header = new Template($headerfile);
        		$result .= $header->Parse();
        	}
        		
            foreach ($flights as $flight)
            {
        		// render table data
                $bodyfile = APPPATH."template/body_search.tpl";
        		if (file_exists($bodyfile))
        		{
        			$body = new Template($bodyfile);
                    $body->set("model", $flight);
        			$result .= $body->Parse();
        		}
            }                    		
            $result .= "</table>";
        }
        else
        {
            $result .= $this->SearchDialog();
        }
        
        return $result;
	}
    
    public function reserve()
	{
        $core = Core::GetInstance();
		$params = $core->route->GetParameters();
		$result = "";
        
        include(APPPATH."model/reservation.php");			
		$reservation = new Reservation();
        $reservation->id = null;
        $reservation->idPerson = "1";
        $reservation->idPersonRole = "1";
        $reservation->idFlight = $params[1];
        $reservation->idCurrency = "CZK";
        $reservation->idOrder = "1";
        $reservation->price = "0";          
        $data_string=json_encode($reservation, JSON_NUMERIC_CHECK );
        var_dump($data_string);
		$ch = curl_init($core->backend_url."/reservation/add");      
		curl_setopt_array($ch, array(
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POST => true,
		CURLOPT_POSTFIELDS => $data_string,
		CURLOPT_HEADER => true,
		CURLOPT_HTTPHEADER => array('Content-Type: application/json', 'Content-Length: ' . strlen($data_string)))
		);
		
		$server_output = curl_exec ($ch);
		curl_close ($ch);
		
		if ($server_output == "OK") 
		{ 
			$result .= "Rezervace byla provedena.";
		} 
		else 
		{ 
			$result .= "Server odpověděl: ".$server_output;
		}
        
        return $result;
    }
    
    public function search_flights()
	{
        $core = Core::GetInstance();
		$params = $core->route->GetParameters();
		$result = "";
        
		if ($_SERVER['REQUEST_METHOD'] === 'POST')
		{	// response for form data
        }
        else
        {
            $result .= $this->SearchReservationsDialog();
        }
        
        return $result;
    }
    
    public function SearchDialog()
    {
        $res = "";
        $src = APPPATH."template/search.tpl";
        if (!file_exists($src))
        {
            $res = "Soubor ".$src." neexistuje";
        }
        else
        {
        	$head = new Template($src);  
        	$res .= "<form action=\"".ROOTPATH."services/reserve_flight/\" method=\"post\">";
        	$res .= $head->Parse();
           	$res .= "</form>";
        }
        return $res;
    }
    
    public function SearchReservationsDialog()
    {
        $res = "";
        $src = APPPATH."template/reservations.tpl";
        if (!file_exists($src))
        {
            $res = "Soubor ".$src." neexistuje";
        }
        else
        {
        	$head = new Template($src);  
        	$res .= "<form action=\"".ROOTPATH."services/search_flights/\" method=\"post\">";
        	$res .= $head->Parse();
           	$res .= "</form>";
        }
        return $res;
    }
}	
?>