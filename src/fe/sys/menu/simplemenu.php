<?php
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) 
{
  exit("You can't access this file directly !");
}

class Menu
{
	public $items = array ();
	
	public function AddItem($item)
	{
		$this->items[] = $item; 
	}
	
	public function Render()
	{
		$res = "<ul>";
		foreach ($this->items as $i => $value) 
		{
			$res .= "<li>".$this->GetMenuItemLink($value->title, $value->link)."</li>";
		}
		$res .= "</ul>";
		return $res;
	}
	
	function GetMenuItemLink($title, $link)
	{
    	return "<a href=\"?id=".$link."\" class=\"left\">".$title."</a>";
	}
}

?>