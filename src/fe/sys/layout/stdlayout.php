<?php
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) 
{
  exit("You can't access this file directly !");
}

class Layout
{
    public $header;
	public $menu;
	public $body;
	//public $footer;
	
	public function __construct()
	{
	}
	
	public function Render(&$content)
	{
	  $content .= "   <header>\n";
	  $content .= $this->header;
	  $content .= "   </header>\n";	  
	  $content .= "   <nav id=\"topNav\">\n";
	  $content .= $this->menu;
      $content .= "   </nav>\n";
	  $content .= "   <div id=\"container\">\n";
	  $content .= $this->body;
	  $content .= "   </div>\n";
	  //$content .= "<footer>\n";
	  //$content .= $this->footer;
	  //$content .= "</footer>\n";
	}
}

?>
