<?php
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) 
{
  exit("You can't access this file directly !");
}

class Template
{
    private $template;

    public function __construct($template = null)
    {
        $this->set("ROOTPATH", ROOTPATH);    
        $this->set("CSSPATH", ROOTPATH."css/");
        $this->set("IMGPATH", ROOTPATH."images/");
        $this->set("JSPATH", ROOTPATH.SYSPATH."script/");
    
        $core = Core::GetInstance();
        //$this->set("APPURL", $core->backend_url);
        $this->set("APPURL", ROOTPATH."backend");
        
        if (isset($template))
        {
            $this->Load($template);
        }
    }

    public function Load($template)
    {
        if (!is_file($template))
        {
            throw new FileNotFoundException("File not found: $template");
        } 
		else if (!is_readable($template))
        {
            throw new IOException("Could not access file: $template");
        } 
		else
        {
            $this->template = $template;
        }
    }

    public function Set($var, $content)
    {
        $this->$var = $content;
    }

    public function Render()
    {
        ob_start();
        require $this->template;
        $content = ob_get_clean();
        print $content;
    }
	
    public function Parse()
    {
        ob_start();
        require $this->template;
        $content = ob_get_clean();
        return $content;
    }
}
