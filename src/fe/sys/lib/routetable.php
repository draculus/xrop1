<?php
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) 
{
  exit("You can't access this file directly !");
}

class RouteTable
{
    var $route;
	
	function RouteTable()
	{
		$requestURI = explode('/', $_SERVER['REQUEST_URI']);
		
		$scriptName = explode('/',$_SERVER['SCRIPT_NAME']);
		
		for($i= 0;$i < sizeof($scriptName);$i++)
		{
			if ($requestURI[$i]	== $scriptName[$i])
			{
				unset($requestURI[$i]);
			}
		}
	
		$controller = array_values($requestURI);
		$parameters = array_slice($controller,1);
		$this->route = new Route($controller[0],$parameters);
	}

	function GetRoute()
	{
		if (!isset($this->route) || (strlen($this->route->GetController())==0))
		{
			$this->route = new Route("main",array());
		}
		
		return $this->route;
	}
}
?>
