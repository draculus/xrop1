<?php
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) 
{
  exit("You can't access this file directly !");
}

class MenuItem
{
	public $title;
	public $link;
	
	public function __construct($title, $link)
	{
		$this->title = $title;
		$this->link = $link;
	}
}

?>