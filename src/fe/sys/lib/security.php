<?php
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) 
{
  exit("You can't access this file directly !");
}

class Security
{	
	var $Database;
	var $UserId;
	var $GroupId;
	var $UserName;	

	function Security($db)
    {
		$this -> Database = $db;
		$UserId = $this -> GetUserId();
		$GroupId = $this -> GetGroupId();
		$this -> SetUserId( $UserId );
		$this -> SetGroupId( $GroupId );	
    }

	function GetPost($Name)
	{
		$return[0] = -1;
		if(isset($_POST[$Name]))
		{
			$return[0] = 1;
			$return[1] = $_POST[$Name];
			$return[1] = str_replace("'","''",$return[1]);
		}
		return $return;
	}

	function GetUserId()
		{
			if(isset($_SESSION['UserId'])) return $_SESSION['UserId'];
			else return -1;
		}

	function GetGroupId()
	{
		if(isset($_SESSION['GroupId'])) return $_SESSION['GroupId'];
		else return -1;
	}

	function GetUserName()
	{
		if(isset($_SESSION['UserName'])) return $_SESSION['UserName'];
		else return "Nikdo";
	}

	function SetUserId($Id)
	{
		$_SESSION['UserId'] = $Id;
		$this -> UserId = $Id;
	}

	function SetGroupId($Id)
	{
		$_SESSION['GroupId'] = $Id;
		$this -> GroupId = $Id;
	}


	function SetUserName($Id)
	{
		$_SESSION['UserName'] = $Id;
		$this -> UserName = $Id;
	}

	function SetValue($Name, $Val)
	{
		$_SESSION[$Name] = $Val;
	}

	function GetValue($Name)
	{
		if(isset($_SESSION[$Name])) return $_SESSION[$Name];
		else return -1;
	}

	function Login($Login, $Passwd)
	{			
			$Result = $this -> Database -> Connector -> SqlQuery("SELECT * FROM tuzivatel WHERE prihl_jmeno='".$Login."' AND heslo='".md5($Passwd)."'");
			$Rows = $this -> Database -> Connector -> SqlNumRows($Result);
			if($Rows == 1)
				{
					$Row = $this -> Database -> Connector -> SqlFetchRow($Result);
					//$this -> SetUserId( $Row['ftuident'] );
					$this -> SetUserId( $Row['id_uzivatel'] );
					// $this -> SetGroupId( $Row['id_group'] ); ... z tcardfile vzit pole skupin nejak
					$this -> SetGroupId( $Row['opravneni']  );

					$this -> SetUserName( $Row['prijmeni']." ".$Row['jmeno'] );
					return 1;
				}
			else
				{
					$this -> SetUserId( -1 ); // 0
					$this -> SetGroupId( -1 ); // 0
					$this -> SetUserName( "" );
					return -1;
				}
	}

	function Logout()
	{
            $this -> SetUserId( -1 );
			$this -> SetGroupId( -1 ); 
			$this -> SetUserName( "" );
			session_destroy();
			return 1;
	}
}

?>