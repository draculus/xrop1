<?
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) 
{
  exit("You can't access this file directly !");
}

class Route
{
        var $controller = '';
        var $parameters = array();
        
        function Route($controller,$parameters)
        {
            $this->controller = $controller;
            $this->parameters = $parameters;
        }
		
        function GetController()
        {
            return $this->controller;
        }
		
        function GetParameters()
        {
            return $this->parameters;
        }
}
?>