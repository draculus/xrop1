<?php
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) 
{
  exit("You can't access this file directly !");
}

class Core
{
	public $title = "Default title";
	public $theme = "common";
	public $content = "";	
	public $layout;
	public $menu;
	public $module;
	public $security;
	public $database;
	public $routetable;
	public $route;
	public $backend_url;
	
	private static $instance = false;
	
    public static function GetInstance()
	{
		if(self::$instance === false)
		{
			self::$instance = new Core();
		}
		return self::$instance;
    }
	
	private function __construct()
	{
        date_default_timezone_set('UTC');
        
		// sessions start
		session_start();			
		
		ob_start();
		
		error_reporting(E_ALL);
		ini_set("display_errors", True); 
		
		// load configuration
		require_once("config.php");
		
		$this->backend_url = $backend_protocol.$backend_host.":".$backend_port.$backend_dir;
		
		// Define common constants
		// SELF	... name of this file
		define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
		
		// EXT ... file extension
		define('EXT', '.php');			
		
		// BASEPATH	... full server path to this file
		define('BASEPATH', str_replace(SELF, '', __FILE__));
		
        // ROOT
        define('ROOTPATH', $root_folder);
        
		// SYSPATH ... path to the "system" folder
		define('SYSPATH', $system_folder);
		
		// APPPATH ... path to the "application" folder		
		define('APPPATH', $application_folder);			
		
		// TITLE ... application title
		$this->title = $application_title;
		
		// THEME ... css file
		$this->theme = APPPATH."theme/".$application_theme;
		
		require_once(SYSPATH."lib/message.php");
		$this->message = new Message();
				
		// Layout
		require_once(SYSPATH."layout/".$application_layout.".php");
		$this->layout = new Layout();
		
		// Database connection
		//require_once(SYSPATH."lib/database.php");
		//$this->database = new DataBase(
		//	$database_User,
		//	$database_Pasw,
		//	$database_Host,
		//	$database_Name,
		//	$database_Type);

		//if($this -> database -> Connected != 1)
		//{
		//	$this -> message -> Display("DATABASE ERROR");			
		//}
		
		require_once(SYSPATH."lib/security.php");
		$this->security = new Security($this->database);
		
		require_once(SYSPATH."lib/template.php");
		
		require_once(SYSPATH."menu/".$application_menu.".php");
		$this->menu = new Menu();
		
		require_once(SYSPATH."lib/route.php");
		require_once(SYSPATH."lib/routetable.php");

		$this->routetable = new RouteTable();
		$this->route = $this->routetable->GetRoute();
		require_once(SYSPATH."lib/controller.php");
		require_once(APPPATH."controller/".$this->route->GetController().".php");
		
		//$this->module = new Module($this);
		//if(class_exists($className))
		$class_name = $this->route->GetController();
		$this->controller = new $class_name;
		$params = $this->route->GetParameters();
		
		if (count($params) > 0)
			$this->controller->action = $params[0];
		
		//set_error_handler('errhandler');  
	}
	
	public function DebugMode()
	{
		error_reporting(E_ALL);
		ini_set("display_errors", 1); 
	}
	
	public function DumpPaths()
	{
        echo "<div style=\"background-color: red\">";
		echo "Defined paths:<br />";
		echo "EXT = ".EXT."<br />";
		echo "SELF = ".SELF."<br />";
		echo "BASEPATH = ".BASEPATH."<br />";
        echo "ROOTPATH = ".ROOTPATH."<br />";
		echo "SYSPATH = ".SYSPATH."<br />";
		echo "APPPATH = ".APPPATH."<br />";	
        echo "</div>";
	}
	
	public function GetLayout()
	{
	    return $this->layout;
	}
	
	public function GetContent($filename) 
	{
		if (is_file($filename)) {
			ob_start();
			include $filename;
			$contents = ob_get_contents();
			ob_end_clean();
			return $contents;
		}
		return false;
	}
	
	public function Render()
	{
		$this->layout->Render($this->content);
		echo("<!doctype html>\n");
		echo("<html lang=\"cz\">\n");
		$head = new Template(APPPATH."template/head.tpl");  
		$head->set("title", $this->title);
		$head->Render();
        echo("<body>\n");
		echo($this->content);
		echo("</body>\n");
		echo("</html>\n");
		ob_end_flush();
	}
	
}

?>
