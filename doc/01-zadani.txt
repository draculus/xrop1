Název projektu
==============
Informační systém pro správu objednávek a klientů (letecké) cestovní
společnosti s názvem Poletucha (název je pracovní, ještě se může změnit,
další návrhy jsou např. ČmelákAir, SkyAndula).

Naše společnost zajišťuje lety (ne však letadla) pro osobní dopravu
soukromého i obchodního charakteru.

Vize -- pracovní verze
====
V novém informačním systému se budou spravovat klienti letecké
společnosti, jejich objednávky, rezervace, nákupy letenek, cílové
destinace a rezervace míst v letadlech, účtování (komunikace s externími
subjekty).
